<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Verifikasi Alamat Email Anda</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            margin: 0;
            padding: 0;
        }
        .container {
            max-width: 600px;
            margin: 0 auto;
            padding: 20px;
            background-color: #ffffff;
            border-radius: 8px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }
        .header {
            text-align: center;
            padding: 10px 0;
            border-bottom: 1px solid #e0e0e0;
        }
        .content {
            padding: 20px;
            line-height: 1.6;
        }
        .button {
            display: inline-block;
            margin: 20px 0;
            padding: 10px 20px;
            color: #ffffff;
            background-color: #3498db;
            text-decoration: none;
            border-radius: 5px;
        }
        .footer {
            text-align: center;
            padding: 10px 0;
            border-top: 1px solid #e0e0e0;
            font-size: 12px;
            color: #777777;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="header">
            <h1>Verifikasi Email</h1>
        </div>
        <div class="content">
            <p>Halo {{ $intern->name }},</p>
            <p>Terima kasih telah mendaftar. Silakan klik tombol di bawah ini untuk memverifikasi alamat email Anda:</p>
            <p>
                <a href="{{ route('verify.email', $intern->verification_token) }}" class="button">Verifikasi Email</a>
            </p>
            <p>Jika Anda tidak membuat akun, tidak perlu melakukan tindakan lebih lanjut.</p>
            <p>Salam,<br>Skillee</p>
        </div>
        <div class="footer">
            <p>&copy; {{ date('Y') }} Skillee. Semua hak dilindungi undang-undang.</p>
        </div>
    </div>
</body>
</html>
