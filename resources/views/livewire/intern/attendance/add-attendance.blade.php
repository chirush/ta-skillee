<main class="flex-1 p-6">
@php
    $internId = Auth::guard('intern')->user()->id;
    $hasAttendedToday = hasAttendedToday($internId);
@endphp
@if($hasAttendedToday)
    <script>
        alert("You have already attended today.");
        window.location.href = "{{ url('/internship/attendances/') }}";
    </script>
@else
    <h1 class="text-3xl font-semibold mb-6">Kehadiran</h1>
    <form action="{{ url('/internship/attendances/add-process') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="mb-4">
            <label for="date" class="block text-gray-700 font-semibold mb-2">Tanggal</label>
            <input type="date" wire:model="date" id="date" name="date" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" max="{{ date('Y-m-d') }}" readonly>
            @error('date') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        <div class="mb-4">
            <label for="status" class="block text-gray-700 font-semibold mb-2">Status</label>
            <select id="status" name="status" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500">
                <option value="Hadir">Hadir</option>
                <option value="Izin">Izin</option>
                <option value="Sakit">Sakit</option>
            </select>
            @error('status') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        <div class="mb-4" id="alasanField" style="display: none;">
            <label for="description" class="block text-gray-700 font-semibold mb-2">Deskripsi</label>
            <textarea id="description" name="description" rows="4" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" placeholder="Masukan deskripsi"></textarea>
            @error('description') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        <div class="card mb-4">
            <label for="name" class="block text-gray-700 font-semibold mb-2">Lokasi</label>
            <div class="card-body">
                <div id="mapid"></div>
            </div>
            <div>
                <button type="button" class="bg-gray-500 hover:bg-gray-700 text-white py-2 px-4 rounded-lg mt-4" onclick="refreshLocation()">Refresh Location</button>
            </div>
        </div>
        <div class="mb-4">
            <label for="picture" class="block text-gray-700 font-semibold mb-2">Foto</label>
            <input type="file" accept="image/*" capture="environment" name="picture" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" required>
            @error('picture') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        <input type="hidden" name="latitude" class="form-control mt-1" id="latitude">
        <input type="hidden" name="longitude" class="form-control mt-1" id="longitude">
        <div>
            <button type="submit" class="bg-gray-800 hover:bg-gray-900 text-white py-2 px-4 rounded-lg">Submit</button>
        </div>
    </form>
</main>

<script>
    window.specificLocationLat = @json($specificLocationLat);
    window.specificLocationLon = @json($specificLocationLon);
</script>
<script src="{{ asset('js/attendance.js') }}"></script>
@endif