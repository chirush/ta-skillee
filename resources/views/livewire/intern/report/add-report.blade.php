<main class="flex-1 p-6">
    <h1 class="text-3xl font-semibold mb-6">Tambah Laporan</h1>
    <form action="{{ url('/internship/reports/add-process') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="mb-4">
            <label for="date" class="block text-gray-700 font-semibold mb-2">Tanggal</label>
            <input type="date" wire:model.lazy="date" id="date" name="date" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" max="{{ date('Y-m-d') }}">
            @error('date') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        <div class="mb-4">
            <label for="name" class="block text-gray-700 font-semibold mb-2">Judul</label>
            <input type="text" wire:model.lazy="title" id="title" name="title" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" required>
            @error('title') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        <div class="mb-4">
            <label for="description" class="block text-gray-700 font-semibold mb-2">Deskripsi</label>
            <textarea wire:model.lazy="description" id="description" name="description" rows="4" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" placeholder="Enter description" required></textarea>
            @error('description') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        <div class="mb-4">
            <label for="picture" class="block text-gray-700 font-semibold mb-2">Foto</label>
            <input type="file" wire:model.lazy="picture" id="picture" name="picture" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" required>
            @error('picture') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        <div>
            <button type="submit" class="bg-gray-800 hover:bg-gray-900 text-white py-2 px-4 rounded-lg">Submit</button>
        </div>
    </form>
</main>
