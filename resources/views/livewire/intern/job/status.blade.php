<main class="flex-1 p-6">
    <h1 class="text-3xl font-semibold mb-6">Status</h1>
    <div class="overflow-x-auto">
        <table id="datatable" class="table-auto min-w-full bg-white shadow-md rounded-lg overflow-hidden">
            <thead class="bg-gray-800 text-white">
                <tr>
                    <th class="px-4 py-2">No</th>
                    <th class="px-4 py-2">Posisi</th>
                    <th class="px-4 py-2">Cabang</th>
                    <th class="px-4 py-2">Tanggal Pendaftaran</th>
                    <th class="px-4 py-2">Tanggal Mulai</th>
                    <th class="px-4 py-2">Status</th>
                </tr>
            </thead>
            <tbody class="text-gray-700">
                @foreach($dataApplications as $index => $application)
                <tr>
                    <td class="border px-4 py-2">{{ $index + 1 }}</td>
                    <td class="border px-4 py-2">{{ $application->job_name }}</td>
                    <td class="border px-4 py-2">{{ $application->branch_name }}</td>
                    <td class="border px-4 py-2">{{ $application->formatted_application_date }}</td>
                    <td class="border px-4 py-2">
                        @if ($application->status == 'Approved')
                            {{ $application->formatted_periode_start }}
                        @else
                            -
                        @endif
                    </td>
                    <td class="border px-4 py-2">
                        @if ($application->status == 'Approved')
                            <span class="text-green-500">Diterima</span>
                        @elseif ($application->status == 'Pending')
                            <span class="text-yellow-500">Pending</span>
                        @elseif ($application->status == 'Declined')
                            <span class="text-red-500">Ditolak</span>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</main>

@script
<script>
    $(document).ready(function() {
        $('#datatable').DataTable({
            "lengthMenu": [[1, 10, 20, 50], [1, 10, 20, 50]],
            "pageLength": 10,
            "searching": true,
            "paging": true,
            "pagingType": "simple_numbers",
            "order": [[0, 'desc']],
        });
    });
</script>
@endscript
