<main class="flex-1 p-6">
    <h1 class="text-3xl font-semibold mb-6">Update Tugas</h1>
    <form action="{{ url('/internship/tasks/update-process/' . $taskId) }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="mb-4">
            <label for="name" class="block text-gray-700 font-semibold mb-2">Judul</label>
            <input type="text" wire:model.lazy="title" id="title" name="title" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" disabled>
            @error('title') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        <div class="mb-4">
            <label for="description" class="block text-gray-700 font-semibold mb-2">Deskripsi</label>
            <textarea wire:model.lazy="description" id="description" name="description" rows="4" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" placeholder="Enter description" disabled></textarea>
            @error('description') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        <div class="mb-4">
            <label for="report" class="block text-gray-700 font-semibold mb-2">Laporan</label>
            <textarea wire:model.lazy="report" id="report" name="report" rows="4" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" placeholder="Enter report" required></textarea>
            @error('report') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        <div class="mb-4">
            <label for="file" class="block text-gray-700 font-semibold mb-2">File</label>
            <input type="file" wire:model.lazy="file" id="file" name="file" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" required>
            @error('file') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        <div class="mb-4">
            <label for="picture" class="block text-gray-700 font-semibold mb-2">Foto</label>
            <input type="file" wire:model.lazy="picture" id="picture" name="picture" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" required>
            @error('picture') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        <div>
            <button type="submit" class="bg-gray-800 hover:bg-gray-900 text-white py-2 px-4 rounded-lg">Submit</button>
        </div>
    </form>
</main>
