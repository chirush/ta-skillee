<main class="flex-1 p-6">
    <h1 class="text-3xl font-semibold mb-6">Tugas</h1>
    <div class="overflow-x-auto">
        <table id="datatable" class="table-auto min-w-full bg-white shadow-md rounded-lg overflow-hidden">
            <thead class="bg-gray-800 text-white">
                <tr>
                    <th class="px-4 py-2">No</th>
                    <th class="px-4 py-2">Judul</th>
                    <th class="px-4 py-2">Deskripsi</th>
                    <th class="px-4 py-2">Batas Waktu</th>
                    <th class="px-4 py-2">Laporan</th>
                    <th class="px-4 py-2">File</th>
                    <th class="px-4 py-2">Foto</th>
                    <th class="px-4 py-2">Catatan Revisi</th>
                    <th class="px-4 py-2">Status</th>
                    <th class="px-4 py-2"></th>
                </tr>
            </thead>
            <tbody class="text-gray-700">
                @foreach($dataTasks as $index => $tasks)
                @php
                if ($tasks->status == "In Progress"){
                    $status = "Sedang Berlanjut";
                }else if ($tasks->status == "Under Review"){
                    $status = "Sedang Ditinjau";
                }else if ($tasks->status == "Completed"){
                    $status = "Selesai";
                }else if ($tasks->status == "Revised"){
                    $status = "Direvisi";
                }
                @endphp
                <tr>
                    <td class="border px-4 py-2">{{ $index + 1 }}</td>
                    <td class="border px-4 py-2">{{ $tasks->title }}</td>
                    <td class="border px-4 py-2">{{ $tasks->description }}</td>
                    <td class="border px-4 py-2">{{ $tasks->deadline }}</td>
                    <td class="border px-4 py-2">{{ $tasks->report }}</td>
                    <td class="border px-4 py-2">
                        @if ($tasks->file)
                        <a href="{{ asset('storage/task') }}/{{ $tasks->file }}" target="_blank" class="text-blue-500">See File</a>
                        @endif
                    </td>
                    <td class="border px-4 py-2">
                        @if ($tasks->picture)
                        <a href="{{ asset('storage/task') }}/{{ $tasks->picture }}" target="_blank" class="text-blue-500">See Picture</a>
                        @endif
                    </td>
                    <td class="border px-4 py-2">{{ $tasks->revise_note}}</td>
                    <td class="border px-4 py-2">{{ $status }}</td>
                    <td class="border px-4 py-2">
                        @if ($status == "Sedang Berlanjut" || $status == "Direvisi")
                        <a href="{{ url('/internship/tasks/update/' . $tasks->id) }}" wire:navigate class="text-blue-700 hover:text-blue-900 mr-2">Update</a>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</main>

@script
<script>
    $(document).ready(function() {
        var table = $('#datatable').DataTable({
            "lengthMenu": [[1, 10, 20, 50], [1, 10, 20, 50]],
            "pageLength": 10,
            "searching": true,
            "paging": true,
            "pagingType": "simple_numbers",
            "order": [[0, 'desc']],
        });
    });
</script>
@endscript
