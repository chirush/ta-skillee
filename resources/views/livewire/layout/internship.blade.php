<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/dt-1.11.5/datatables.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">

    <title>Magang Telkom</title>    

    @vite(['resources/css/app.css','resources/js/app.js'])

    @livewireStyles

    <style>
        body {
            margin: 0;
            padding: 0;
            height: 100%;
            font-family: 'Inter', sans-serif;
        }

        .flex-container {
            display: flex;
            height: 100%;
        }

        aside {
            flex-shrink: 0;
            min-height: 100vh;
        }

        #mapid {
            aspect-ratio: 1/1;
            width: 50%;
            border-radius: 10px;
            z-index: 0;
            height: 300px;
            margin-right: 20px;
        }

        .dropdown-menu {
            display: none;
        }

        .dataTables_paginate {
            white-space: nowrap;
        }

        .dataTables_paginate .paginate_button {
            display: inline-block;
            margin-right: 0.5rem;
        }
    </style>
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;600&display=swap" rel="stylesheet">
</head>
<body class="bg-gray-50 text-gray-800">
    @if (session('error') || session('status'))
        <div id="notification" class="{{ session('error') ? 'error' : 'status' }}">
            {{ session('error') ?? session('status') }}
        </div>

        <script>
            document.addEventListener('DOMContentLoaded', (event) => {
                var notification = document.getElementById("notification");
                notification.style.display = "block";

                setTimeout(function() {
                    notification.style.display = "none";
                }, 5000);
            });
        </script>

        <style>
            #notification {
                position: fixed;
                top: 10%;
                left: 50%;
                transform: translateX(-50%);
                padding: 10px 20px;
                border-radius: 5px;
                z-index: 9999;
                display: none;
                color: #fff;
            }

            #notification.error {
                background-color: #f44336;
            }

            #notification.status {
                background-color: #4caf50;
            }
        </style>
    @endif
    <header class="bg-white text-gray-800 p-4 shadow-md flex justify-between items-center">
        <h1 class="text-xl font-semibold ml-[70px]">SKILLEE</h1>
        <div class="flex items-center relative">
            @if(!Auth::guard('intern')->user()->picture)
            <img src="https://www.gravatar.com/avatar/?d=mp" alt="Profile Picture" class="w-10 h-10 rounded-full mr-2">
            @else
            <img src="{{ asset('storage/picture') }}/{{ Auth::guard('intern')->user()->picture }}" alt="Profile Picture" class="w-10 h-10 rounded-full mr-2">
            @endif
            <div class="ml-2">
                <div class="text-base font-semibold">{{ Auth::guard('intern')->user()->name }}</div>
                @if(!Auth::guard('intern')->user()->position)
                    <div class="text-xs text-gray-500">Applicant</div>
                @else
                    <div class="text-xs text-gray-500">{{ Auth::guard('intern')->user()->position }}</div>
                    <div class="text-xs text-gray-500">{{ DB::table('branches')->where('id', Auth::guard('intern')->user()->branch_id)->value('name') }}</div>
                @endif
            </div>
            <div class="relative ml-4">
                <button id="dropdownButton" class="text-gray-800 py-2 px-4 rounded-lg focus:outline-none">
                    <i class="fas fa-caret-down"></i>
                </button>
                <div id="dropdownMenu" class="absolute right-0 mt-2 w-48 bg-white rounded-lg shadow-lg hidden">
                    <a href="{{ url('/internship/profile') }}" class="block px-4 py-2 text-sm text-gray-800 hover:bg-gray-100">Profile</a>
                    <a href="{{ url('/logout') }}" class="block px-4 py-2 text-sm text-gray-800 hover:bg-gray-100">Logout</a>
                </div>
            </div>
        </div>
    </header>
    <div class="flex-container">
            <aside class="bg-white text-gray-800 w-64 shadow-md">
                <div class="p-4">
                    @if (Auth::guard('intern')->user()->branch_id && Auth::guard('intern')->user()->date_start <= now())
                    @php
                        $internId = Auth::guard('intern')->user()->id;
                        $hasAttendedToday = hasAttendedToday($internId);
                        $countInProgressAndRevisedTasks = countInProgressAndRevisedTasks();
                    @endphp
                    <a href="{{ url('/internship/attendances') }}" wire:navigate class="mt-2 flex items-center justify-between px-4 py-2 text-sm font-semibold rounded-lg hover:bg-gray-100 focus:outline-none focus:bg-gray-100">
                        <span class="flex items-center">
                            <i class="fas fa-message mr-2 w-[20px]"></i>
                            Kehadiran
                        </span>
                        @if(!$hasAttendedToday)
                        <span class="inline-flex items-center justify-center px-2 py-1 text-xs leading-none text-white bg-orange-500 rounded-full">!</span>
                        @endif
                    </a>
                    <a href="{{ url('/internship/reports') }}" wire:navigate class="mt-2 block px-4 py-2 text-sm font-semibold rounded-lg hover:bg-gray-100 focus:outline-none focus:bg-gray-100">
                        <i class="fas fa-file-alt mr-2 w-[20px]"></i>
                         Laporan</a>
                    <a href="{{ url('/internship/tasks') }}" wire:navigate class="mt-2 flex items-center justify-between px-4 py-2 text-sm font-semibold rounded-lg hover:bg-gray-100 focus:outline-none focus:bg-gray-100">
                        <span class="flex items-center">
                            <i class="fas fa-tasks mr-2 w-[20px]"></i>
                            Tugas
                        </span>
                        @if($countInProgressAndRevisedTasks >= 1)
                        <span class="inline-flex items-center justify-center px-2 py-1 text-xs leading-none text-white bg-orange-500 rounded-full">{{ $countInProgressAndRevisedTasks }}</span>
                        @endif
                    </a>
                    @else
                    <a href="{{ url('/internship/jobs') }}" wire:navigate class="mt-2 block px-4 py-2 text-sm font-semibold rounded-lg hover:bg-gray-100 focus:outline-none focus:bg-gray-100">
                        <i class="fas fa-briefcase mr-2 w-[20px]"></i>
                         Lowongan
                     </a>
                    <a href="{{ url('/internship/jobs/status') }}" wire:navigate class="mt-2 block px-4 py-2 text-sm font-semibold rounded-lg hover:bg-gray-100 focus:outline-none focus:bg-gray-100">
                        <i class="fas fa-check-circle mr-2 w-[20px]"></i>
                         Status
                     </a>
                    @endif
                </div>
            </aside>
        
        <main class="p-4 flex-1">
            {{ $slot }}
        </main>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.11.5/datatables.min.js"></script>
        <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"></script>
        
        <script>
            document.getElementById('dropdownButton').addEventListener('click', function() {
                var dropdownMenu = document.getElementById('dropdownMenu');
                dropdownMenu.classList.toggle('hidden');
            });

            window.addEventListener('click', function(event) {
                var dropdownMenu = document.getElementById('dropdownMenu');
                var dropdownButton = document.getElementById('dropdownButton');
                if (!dropdownButton.contains(event.target) && !dropdownMenu.contains(event.target)) {
                    dropdownMenu.classList.add('hidden');
                }
            });
        </script>

        @livewireScripts
    </div>
</body>
</html>
