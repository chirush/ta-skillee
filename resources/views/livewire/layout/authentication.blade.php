<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Magang Telkom</title>
    
	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&amp;display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@splidejs/splide@latest/dist/css/splide.min.css">

    @vite(['resources/css/app.css','resources/js/app.js'])

    @livewireStyles
    <style>
        body {
            font-family: 'Poppins', sans-serif;
        }
    </style>
</head>

    <body class="bg-gray-200 flex justify-center items-center">
    <div class="bg-white rounded-lg shadow-lg flex login-container overflow-hidden h-screen w-full">
        <div class="w-1/3 image-container h-screen hidden md:flex items-center justify-center bg-no-repeat bg-cover" style="background-image: url('{{ asset('/img/bg.png') }}');">
            <a href="javascript:window.history.back()" class="absolute top-0 left-0 m-4 px-2 py-1">
                <i class="fas fa-arrow-left text-white"></i>
            </a>
            <div id="image-carousel" class="splide">
                <div class="splide__track mb-6">
                    <ul class="splide__list h-[300px]">
                        <li class="splide__slide flex justify-center"><img src="{{ asset('/img/splide1.png') }}"></li>
                        <li class="splide__slide flex justify-center"><img src="{{ asset('/img/splide2.png') }}"></li>
                    </ul>
                </div>
            </div>
        </div>
        {{ $slot }}
    </div>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@splidejs/splide@latest/dist/js/splide.min.js"></script>
    <script>
        document.addEventListener('DOMContentLoaded', function () {
            new Splide('#image-carousel', {
                type: 'loop',
                perPage: 1,
                height: '100%',
                arrows: false,
                pagination: true,
                autoplay: true,
                interval: 3000,
            }).mount();
        });
    </script>


    @livewireScripts
</body>

</html>
