<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/dt-1.11.5/datatables.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">

    <title>Magang Telkom</title>    

    @vite(['resources/css/app.css','resources/js/app.js'])

    @livewireStyles

    <style>
        body {
            margin: 0;
            padding: 0;
            height: 100%;
            font-family: 'Inter', sans-serif;
        }

        .flex-container {
            display: flex;
            height: 100%;
        }

        aside {
            flex-shrink: 0;
            min-height: 100vh;
        }

        #mapid {
            aspect-ratio: 1/1;
            width: 50%;
            border-radius: 10px;
            z-index: 0;
            height: 300px;
            margin-right: 20px;
        }

        .dropdown-menu {
            display: none;
        }

        .dataTables_paginate {
            white-space: nowrap;
        }

        .dataTables_paginate .paginate_button {
            display: inline-block;
            margin-right: 0.5rem;
        }
    </style>
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;600&display=swap" rel="stylesheet">
</head>
<body class="bg-gray-50 text-gray-800">
    @if (session('error') || session('status'))
        <div id="notification" class="{{ session('error') ? 'error' : 'status' }}">
            {{ session('error') ?? session('status') }}
        </div>

        <script>
            document.addEventListener('DOMContentLoaded', (event) => {
                var notification = document.getElementById("notification");
                notification.style.display = "block";

                setTimeout(function() {
                    notification.style.display = "none";
                }, 5000);
            });
        </script>

        <style>
            #notification {
                position: fixed;
                top: 10%;
                left: 50%;
                transform: translateX(-50%);
                padding: 10px 20px;
                border-radius: 5px;
                z-index: 9999;
                display: none;
                color: #fff;
            }

            #notification.error {
                background-color: #f44336;
            }

            #notification.status {
                background-color: #4caf50;
            }
        </style>
    @endif
    @php
        $unreadMessageCount = unreadMessageCount();
        $pendingApplicationCount = pendingApplicationCount();
    @endphp
    <header class="bg-white text-gray-800 p-4 shadow-md flex justify-between items-center">
        <h1 class="text-xl font-semibold ml-[70px]">SKILLEE</h1>
        <div class="flex items-center relative">
            @if(!Auth::user()->picture)
            <img src="https://www.gravatar.com/avatar/?d=mp" alt="Profile Picture" class="w-10 h-10 rounded-full mr-2">
            @else
            <img src="{{ asset('storage/picture') }}/{{ Auth::user()->picture }}" alt="Profile Picture" class="w-10 h-10 rounded-full mr-2">
            @endif
            <div class="ml-2">
                <div class="text-base font-semibold">{{ Auth::user()->name }}</div>
                <div class="text-xs text-gray-500">{{ Auth::user()->role }}</div>
            </div>
            <div class="relative ml-4">
                <button id="dropdownButton" class="text-gray-800 py-2 px-4 rounded-lg focus:outline-none">
                    <i class="fas fa-caret-down"></i>
                </button>
                <div id="dropdownMenu" class="absolute right-0 mt-2 w-48 bg-white rounded-lg shadow-lg hidden">
                    <a href="{{ url('/logout') }}" class="block px-4 py-2 text-sm text-gray-800 hover:bg-gray-100">Logout</a>
                </div>
            </div>
        </div>
    </header>
    <div class="flex-container">
        <aside class="bg-white text-gray-800 w-64 shadow-md">
            <div class="p-4">
                <a href="{{ url('/admin/messages') }}" wire:navigate class="mt-2 flex items-center justify-between px-4 py-2 text-sm font-semibold rounded-lg hover:bg-gray-100 focus:outline-none focus:bg-gray-100">
                    <span class="flex items-center">
                        <i class="fas fa-envelope mr-2 w-[20px]"></i>
                        Message
                    </span>
                    @if($unreadMessageCount >= 1)
                    <span class="inline-flex items-center justify-center px-2 py-1 text-xs leading-none text-white bg-orange-500 rounded-full">{{ $unreadMessageCount }}</span>
                    @endif
                </a>
                @if (Auth::user()->role != "Instructor")
                <div x-data="{ open: false }" class="relative">
                    <a href="#" @click.prevent="open = !open" class="mt-2 flex items-center justify-between px-4 py-2 text-sm font-semibold rounded-lg hover:bg-gray-100 focus:outline-none focus:bg-gray-100">
                        <span class="flex items-center">
                            <i class="fas fa-briefcase mr-2 w-[20px]"></i>
                            Internship
                        </span>
                        <i :class="open ? 'fas fa-chevron-up' : 'fas fa-chevron-down'" class="ml-2"></i>
                    </a>
                    <div x-show="open">
                        <a href="{{ url('/admin/jobs') }}" wire:navigate class="mt-1 block px-4 py-2 text-sm rounded-lg hover:bg-gray-100 focus:outline-none focus:bg-gray-100">
                            Jobs
                        </a>
                        <a href="{{ url('/admin/applications') }}" wire:navigate class="mt-1 flex items-center justify-between px-4 py-2 text-sm rounded-lg hover:bg-gray-100 focus:outline-none focus:bg-gray-100">
                            <span class="flex items-center">
                                Applications
                            </span>
                            @if($pendingApplicationCount >= 1)
                            <span class="inline-flex items-center justify-center px-2 py-1 text-xs leading-none text-white bg-orange-500 rounded-full">{{ $pendingApplicationCount }}</span>
                            @endif
                        </a>
                    </div>
                </div>
                @endif
                <div x-data="{ open: false }" class="relative">
                    <a href="#" @click.prevent="open = !open" class="mt-2 flex items-center justify-between px-4 py-2 text-sm font-semibold rounded-lg hover:bg-gray-100 focus:outline-none focus:bg-gray-100">
                        <span class="flex items-center">
                            <i class="fas fa-chart-line mr-2 w-[20px]"></i>
                            Monitor
                        </span>
                        <i :class="open ? 'fas fa-chevron-up' : 'fas fa-chevron-down'" class="ml-2"></i>
                    </a>
                    <div x-show="open">
                        <a href="{{ url('/admin/attendances') }}" wire:navigate class="mt-1 block px-4 py-2 text-sm rounded-lg hover:bg-gray-100 focus:outline-none focus:bg-gray-100">
                            Attendances
                        </a>
                        <a href="{{ url('/admin/reports') }}" wire:navigate class="mt-1 block px-4 py-2 text-sm rounded-lg hover:bg-gray-100 focus:outline-none focus:bg-gray-100">
                            Reports
                        </a>
                    </div>
                </div>
                @if (Auth::user()->role != "Instructor")
                <div x-data="{ open: false }" class="relative">
                    <a href="#" @click.prevent="open = !open" class="mt-2 flex items-center justify-between px-4 py-2 text-sm font-semibold rounded-lg hover:bg-gray-100 focus:outline-none focus:bg-gray-100">
                        <span class="flex items-center">
                            <i class="fas fa-users mr-2 w-[20px]"></i>
                            User
                        </span>
                        <i :class="open ? 'fas fa-chevron-up' : 'fas fa-chevron-down'" class="ml-2"></i>
                    </a>
                    <div x-show="open">
                        @if (Auth::user()->role == "Superadmin")
                        <a href="{{ url('/admin/users') }}" wire:navigate class="mt-1 block px-4 py-2 text-sm rounded-lg hover:bg-gray-100 focus:outline-none focus:bg-gray-100">
                            Users
                        </a>
                        @endif
                        <a href="{{ url('/admin/interns') }}" wire:navigate class="mt-1 block px-4 py-2 text-sm rounded-lg hover:bg-gray-100 focus:outline-none focus:bg-gray-100">
                            Interns
                        </a>
                    </div>
                </div>
                <div x-data="{ open: false }" class="relative">
                    <a href="#" @click.prevent="open = !open" class="mt-2 flex items-center justify-between px-4 py-2 text-sm font-semibold rounded-lg hover:bg-gray-100 focus:outline-none focus:bg-gray-100">
                        <span class="flex items-center">
                            <i class="fas fa-database mr-2 w-[20px]"></i>
                            Master Data
                        </span>
                        <i :class="open ? 'fas fa-chevron-up' : 'fas fa-chevron-down'" class="ml-2"></i>
                    </a>
                    <div x-show="open">
                        @if (Auth::user()->role == "Superadmin")
                        <a href="{{ url('/admin/branches') }}" wire:navigate class="mt-1 block px-4 py-2 text-sm rounded-lg hover:bg-gray-100 focus:outline-none focus:bg-gray-100">
                            Branches
                        </a>
                        @endif
                        <a href="{{ url('/admin/instances') }}" wire:navigate class="mt-1 block px-4 py-2 text-sm rounded-lg hover:bg-gray-100 focus:outline-none focus:bg-gray-100">
                            Instances
                        </a>
                    </div>
                </div>
                @endif
                @if (Auth::user()->role == "Instructor")
                @php
                $countUnderReviewTasks = countUnderReviewTasks()
                @endphp
                <a href="{{ url('/admin/tasks') }}" wire:navigate class="mt-2 flex items-center justify-between px-4 py-2 text-sm font-semibold rounded-lg hover:bg-gray-100 focus:outline-none focus:bg-gray-100">
                    <span class="flex items-center">
                        <i class="fas fa-tasks mr-2 w-[20px]"></i>
                        Tasks
                    </span>
                    @if($countUnderReviewTasks >= 1)
                    <span class="inline-flex items-center justify-center px-2 py-1 text-xs leading-none text-white bg-orange-500 rounded-full">{{ $countUnderReviewTasks }}</span>
                    @endif
                </a>
                @endif
            </div>
        </aside>

        
        <main class="p-4 flex-1">
            {{ $slot }}
        </main>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.11.5/datatables.min.js"></script>
        <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"></script>
        
        <script>
            document.getElementById('dropdownButton').addEventListener('click', function() {
                var dropdownMenu = document.getElementById('dropdownMenu');
                dropdownMenu.classList.toggle('hidden');
            });

            window.addEventListener('click', function(event) {
                var dropdownMenu = document.getElementById('dropdownMenu');
                var dropdownButton = document.getElementById('dropdownButton');
                if (!dropdownButton.contains(event.target) && !dropdownMenu.contains(event.target)) {
                    dropdownMenu.classList.add('hidden');
                }
            });
        </script>

        @livewireScripts
    </div>
</body>
</html>
