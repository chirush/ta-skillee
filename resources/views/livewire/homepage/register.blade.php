<div class="w-1/1 md:w-2/3 p-8 h-full overflow-y-auto" style="max-height: 100vh;">
    <h2 class="text-2xl font-bold mb-4 text-gray-700 md:text-3xl">Ambil peluang ini: Daftarkan diri Anda sekarang untuk program magang kami!</h2>
    <form wire:submit.prevent="processRegistration">
        <div class="mb-4">
            <label for="program_type" class="block text-gray-700 font-semibold mb-2">Jenis Magang</label>
            <select id="program_type" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" wire:model.lazy="programType" wire:change="fetchInstances" disabled>
                <option value="High School">Program Magang SMA/SMK</option>
                <option value="University">Program Magang Universitas</option>
            </select>
        </div>
        @if ($programType === 'High School')
        <div class="mb-4">
            <div class="grid grid-cols-2 gap-4">
                <div>
                    <label for="instance" class="block text-gray-700 font-semibold mb-2">Instansi</label>
                    <select id="instance" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" wire:model.lazy="instance">
                        <option value="null" selected disabled>{{ __('Please select') }}</option>
                        @foreach($dataInstance as $instance)
                            <option value="{{ $instance->id }}">{{ $instance->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div>
                    <label for="program" class="block text-gray-700 font-semibold mb-2">Program</label>
                    <input type="text" id="Program" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" placeholder="Masukkan nama program studi" wire:model.lazy="program">
                </div>
            </div>
        </div>
        @else
        <div class="mb-4">
            <label for="instance" class="block text-gray-700 font-semibold mb-2">Instansi</label>
            <select id="instance" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" wire:model="instance">
                <option value="null" selected disabled>{{ __('Please select') }}</option>
                @foreach($dataInstance as $instance)
                    <option value="{{ $instance->id }}">{{ $instance->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="mb-4">
            <div class="grid grid-cols-2 gap-4">
                <div>
                    <label for="faculty" class="block text-gray-700 font-semibold mb-2">Fakultas</label>
                    <input type="text" id="Faculty" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" placeholder="Masukkan fakultas" wire:model.lazy="faculty">
                </div>
                <div>
                    <label for="program" class="block text-gray-700 font-semibold mb-2">Program</label>
                    <input type="text" id="Program" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" placeholder="Masukkan program studi" wire:model.lazy="program">
                </div>
            </div>
        </div>
        @endif
        <div class="mb-4">
            <div class="grid grid-cols-2 gap-4">
                <div>
                    <label for="name" class="block text-gray-700 font-semibold mb-2">Nama</label>
                    <input type="text" id="name" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" placeholder="Masukkan nama anda" wire:model.lazy="name">
                    @error('name')
                        <span class="text-red-600" style="font-size: 12.5px;">{{ $message }}</span>
                    @enderror
                </div>
                <div>
                    <label for="nik" class="block text-gray-700 font-semibold mb-2">NIK</label>
                    <input type="text" id="nik" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" placeholder="Masukkan NIK anda" wire:model.lazy="nik">
                    @error('nik')
                        <span class="text-red-600" style="font-size: 12.5px;">{{ $message }}</span>
                    @enderror
                </div>
            </div>
        </div>
        <div class="mb-4">
            <div class="grid grid-cols-2 gap-4">
                <div>
                    <label for="email" class="block text-gray-700 font-semibold mb-2">Email</label>
                    <input type="email" id="email" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" placeholder="Masukkan email anda" wire:model.lazy="email">
                    @error('email')
                        <span class="text-red-600" style="font-size: 12.5px;">{{ $message }}</span>
                    @enderror
                </div>
                <div>
                    <label for="phone" class="block text-gray-700 font-semibold mb-2">Nomor Telepon</label>
                    <input type="text" id="phone" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" placeholder="Masukkan nomor telepon anda" wire:model.lazy="phone">
                    @error('phone')
                        <span class="text-red-600" style="font-size: 12.5px;">{{ $message }}</span>
                    @enderror
                </div>
            </div>
        </div>
        <div class="mb-4">
            <div class="grid grid-cols-2 gap-4">
                <div>
                    <label for="password" class="block text-gray-700 font-semibold mb-2">Password</label>
                    <div class="relative">
                        <input type="password" id="password" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" placeholder="Masukkan password anda" wire:model.lazy="password">
                        <button type="button" id="password-toggle" class="absolute top-0 right-0 mr-4 mt-3 text-sm text-gray-600 focus:outline-none" onclick="togglePasswordVisibility('password')">
                            <i class="far fa-eye" id="password-icon"></i>
                        </button>
                    </div>
                    @error('password')
                        <span class="text-red-600" style="font-size: 12.5px;">{{ $message }}</span>
                    @enderror
                </div>
                <div>
                    <label for="password_confirmation" class="block text-gray-700 font-semibold mb-2">Password Confirmation</label>
                    <div class="relative">
                        <input type="password" id="password_confirmation" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" placeholder="Masukkan password konfirmasi anda" wire:model.lazy="password_confirmation">
                        <button type="button" id="password_confirmation-toggle" class="absolute top-0 right-0 mr-4 mt-3 text-sm text-gray-600 focus:outline-none" onclick="togglePasswordVisibility('password_confirmation')">
                            <i class="far fa-eye" id="password_confirmation-icon"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="mb-4">
            <div class="grid grid-cols-3 gap-4">
                <div>
                    <label for="province" class="block text-gray-700 font-semibold mb-2">Provinsi</label>
                    <select id="province" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" wire:model.lazy="province" wire:change="fetchRegency">
                        <option value="null" selected disabled>{{ __('Please select') }}</option>
                        @foreach($dataProvinces as $province)
                            <option value="{{ $province->id }}">{{ $province->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div>
                    <label for="regency" class="block text-gray-700 font-semibold mb-2">Kabupaten</label>
                    <select id="regency" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" wire:model.lazy="regency">
                        <option value="null" selected disabled>{{ __('Please select') }}</option>
                        @foreach($regencies as $regency)
                            <option value="{{ $regency->id }}">{{ $regency->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div>
                    <label for="district" class="block text-gray-700 font-semibold mb-2">Kecamatan</label>
                    <select id="district" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" wire:model.lazy="district">
                        <option value="null" selected disabled>{{ __('Please select') }}</option>
                        @foreach($districts  as $district)
                            <option value="{{ $district->id }}">{{ $district->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

        <button type="submit" class="w-full bg-blue-500 text-white py-2 rounded-lg hover:bg-blue-600 focus:outline-none focus:bg-blue-600">Daftar</button>
        <div class="mt-4 text-sm text-gray-600">
            Sudah memiliki akun? <a href="{{ url('/login') }}" class="text-blue-500">Login disini</a>
        </div>
    </form>
</div>

<script>
    function togglePasswordVisibility(field) {
        var passwordInput = document.getElementById(field);
        var passwordIcon = document.getElementById(field + "-icon");

        if (passwordInput.type === "password") {
            passwordInput.type = "text";
            passwordIcon.classList.remove("fa-eye");
            passwordIcon.classList.add("fa-eye-slash");
        } else {
            passwordInput.type = "password";
            passwordIcon.classList.remove("fa-eye-slash");
            passwordIcon.classList.add("fa-eye");
        }
    }
</script>
