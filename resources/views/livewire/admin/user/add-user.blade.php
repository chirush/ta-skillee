<main class="flex-1 p-6">
    <h1 class="text-3xl font-semibold mb-6">Tambah User</h1>
    <form wire:submit.prevent="addUser">
        <div class="mb-4">
            <label for="name" class="block text-gray-700 font-semibold mb-2">Nama</label>
            <input wire:model.lazy="name" type="text" id="name" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" placeholder="Enter name">
            @error('name') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        <div class="mb-4">
            <label for="email" class="block text-gray-700 font-semibold mb-2">Email</label>
            <input wire:model.lazy="email" type="email" id="email" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" placeholder="Enter email">
            @error('email') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        <div class="mb-4">
            <label for="password" class="block text-gray-700 font-semibold mb-2">Password</label>
            <input wire:model.lazy="password" type="password" id="password" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" placeholder="Enter password">
            @error('password') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        <div class="mb-4">
            <label for="phone" class="block text-gray-700 font-semibold mb-2">Telepon</label>
            <input wire:model.lazy="phone" type="text" id="phone" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" placeholder="Enter phone">
            @error('phone') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        <div class="mb-4">
            <label for="role" class="block text-gray-700 font-semibold mb-2">Role</label>
            <select wire:model.lazy="role" id="role" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" required>
                <option value="null" selected disabled>Select Role</option>
                @if (Auth::user()->role != "Human Resources")
                <option value="Superadmin">Superadmin</option>
                <option value="Human Resources">Human Resources</option>
                @endif
                <option value="Instructor">Instructor</option>
            </select>
            @error('role') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        @php
        $allBranch = getAllBranch();
        @endphp
        @if (Auth::user()->role == "Superadmin")
        <div class="mb-4">
            <label for="branch" class="block text-gray-700 font-semibold mb-2">Cabang</label>
            <select wire:model.lazy="branch" id="branch" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" required>
                <option value="null" selected disabled>Pilih Cabang</option>
                @foreach ($allBranch as $branches)
                <option value="{{ $branches->id }}">{{ $branches->name }}</option>
                @endforeach
            </select>
            @error('branch') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        @endif
        <div>
            <button type="submit" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-lg">Simpan</button>
        </div>
    </form>
</main>
