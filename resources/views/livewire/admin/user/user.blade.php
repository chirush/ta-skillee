<main class="flex-1 p-6">
    <h1 class="text-3xl font-semibold mb-6">Users</h1>
    <div class="mb-6">
        <a href="{{ url('/admin/users/add') }}" wire:navigate class="bg-gray-800 hover:bg-gray-900 text-white py-2 px-4 rounded-lg">+ Add User</a>
    </div>
    <div class="overflow-x-auto">
        <table id="datatable" class="table-auto min-w-full bg-white shadow-md rounded-lg overflow-hidden">
            <thead class="bg-gray-800 text-white">
                <tr>
                    <th class="px-4 py-2">ID</th>
                    <th class="px-4 py-2">Branch</th>
                    <th class="px-4 py-2">Email</th>
                    <th class="px-4 py-2">Name</th>
                    <th class="px-4 py-2">Phone</th>
                    <th class="px-4 py-2">Role</th>
                </tr>
            </thead>
            <tbody class="text-gray-700">
                @foreach($dataUsers as $users)
                <tr>
                    <td class="border px-4 py-2">{{ $users->id }}</td>
                    <td class="border px-4 py-2">{{ $users->branch_name }}</td>
                    <td class="border px-4 py-2">{{ $users->email }}</td>
                    <td class="border px-4 py-2">{{ $users->name }}</td>
                    <td class="border px-4 py-2">{{ $users->phone }}</td>
                    <td class="border px-4 py-2">{{ $users->role }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</main>

@script
<script>
    $(document).ready(function() {
        var table = $('#datatable').DataTable({
            "lengthMenu": [[5, 10, 20, 50], [5, 10, 20, 50]],
            "pageLength": 10,
            "searching": true,
            "paging": true,
            "pagingType": "simple_numbers",
            "order": [[0, 'desc']],
        });
    });
</script>
@endscript