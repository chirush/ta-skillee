<main class="flex-1 p-6">
    <h1 class="text-3xl font-semibold mb-6">Interns</h1>
    <div class="mb-2 text-right">
        @php
        $allBranch = getAllBranch();
        @endphp
        @if (Auth::user()->role == "Superadmin")
        <label for="branch" class="mr-2">Filter by Branch:</label>
        <select id="branch" class="bg-white border border-gray-300 rounded px-2 py-1 text-sm">
            <option value="all">All</option>
            @foreach ($allBranch as $branches)
            <option value="{{ $branches->name }}">{{ $branches->name }}</option>
            @endforeach
        </select>
        @endif
    </div>
    <div class="overflow-x-auto">
        <table id="datatable" class="table-auto min-w-full bg-white shadow-md rounded-lg overflow-hidden">
            <thead class="bg-gray-800 text-white">
                <tr>
                    <th class="px-4 py-2">ID</th>
                    <th class="px-4 py-2">Branch</th>
                    <th class="px-4 py-2">Instructor</th>
                    <th class="px-4 py-2">Instance</th>
                    <th class="px-4 py-2">Position</th>
                    <th class="px-4 py-2">Name</th>
                    <th class="px-4 py-2">Email</th>
                    <th class="px-4 py-2">Phone</th>
                    <th class="px-4 py-2">NIK</th>
                    @if (Auth::user()->role == "Human Resources")
                    <th class="px-4 py-2"></th>
                    @endif
                </tr>
            </thead>
            <tbody class="text-gray-700">
                @foreach($dataInterns as $interns)
                <tr>
                    <td class="border px-4 py-2">{{ $interns->id }}</td>
                    <td class="border px-4 py-2">{{ $interns->branch_name }}</td>
                    <td class="border px-4 py-2">{{ $interns->instructor_name }}</td>
                    <td class="border px-4 py-2">{{ $interns->instance_name }}</td>
                    <td class="border px-4 py-2">{{ $interns->position }}</td>
                    <td class="border px-4 py-2">{{ $interns->name }}</td>
                    <td class="border px-4 py-2">{{ $interns->email }}</td>
                    <td class="border px-4 py-2">{{ $interns->phone }}</td>
                    <td class="border px-4 py-2">{{ $interns->nik }}</td>
                    @if (Auth::user()->role == "Human Resources")
                    <td class="border px-4 py-2">
                        <a href="{{ url('/admin/certificate/' . $interns->id) }}" class="text-blue-700 hover:text-blue-900 mr-2">Certificate</a>
                    </td>
                    @endif
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</main>

@script
<script>
    $(document).ready(function() {
        var table = $('#datatable').DataTable({
            "lengthMenu": [[5, 10, 20, 50], [5, 10, 20, 50]],
            "pageLength": 10,
            "searching": true,
            "paging": true,
            "pagingType": "simple_numbers",
            "order": [[0, 'desc']],
        });

        $('#branch').on('change', function() {
            var branch = $(this).val().toLowerCase();
            if (branch === "all") {
                table.column(1).search('').draw();
            } else {
                table.column(1).search(branch).draw();
            }
        });
    });
</script>
@endscript