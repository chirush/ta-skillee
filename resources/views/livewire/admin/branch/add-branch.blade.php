<main class="flex-1 p-6">
    <h1 class="text-3xl font-semibold mb-6">Add Branch</h1>
    <form wire:submit.prevent="addBranch">
        <div class="mb-4">
            <label for="name" class="block text-gray-700 font-semibold mb-2">Name</label>
            <input wire:model.lazy="name" type="text" id="name" name="name" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" placeholder="Enter name">
            @error('name') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        <div class="mb-4">
            <label for="contact" class="block text-gray-700 font-semibold mb-2">Contact</label>
            <input wire:model.lazy="contact" type="text" id="contact" name="contact" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" placeholder="Enter contact">
            @error('contact') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        <div class="mb-4">
            <label for="address" class="block text-gray-700 font-semibold mb-2">Address</label>
            <textarea wire:model.lazy="address" id="address" name="address" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" placeholder="Enter address"></textarea>
        </div>
        <div class="mb-4">
            <label for="latitude" class="block text-gray-700 font-semibold mb-2">Latitude</label>
            <input wire:model.lazy="latitude" type="text" id="latitude" name="latitude" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" placeholder="Enter latitude">
            @error('latitude') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        <div class="mb-4">
            <label for="longitude" class="block text-gray-700 font-semibold mb-2">Longitude</label>
            <input wire:model.lazy="longitude" type="text" id="longitude" name="longitude" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" placeholder="Enter longitude">
            @error('longitude') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        <div>
            <button type="submit" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-lg">Submit</button>
        </div>
    </form>
</main>
