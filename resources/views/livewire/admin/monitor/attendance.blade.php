<main class="flex-1 p-6">
    <h1 class="text-3xl font-semibold mb-6">Attendance</h1>
    <div class="mb-2 text-right">
        @php
        $allBranch = getAllBranch();
        @endphp
        @if (Auth::user()->role == "Superadmin")
        <label for="branch" class="mr-2">Filter by Branch:</label>
        <select id="branch" class="bg-white border border-gray-300 rounded px-2 py-1 text-sm">
            <option value="all">All</option>
            @foreach ($allBranch as $branches)
            <option value="{{ $branches->name }}">{{ $branches->name }}</option>
            @endforeach
        </select>
        @endif
    </div>
    <div class="overflow-x-auto">
        <table id="datatable" class="table-auto min-w-full bg-white shadow-md rounded-lg overflow-hidden">
            <thead class="bg-gray-800 text-white">
                <tr>
                    <th class="px-4 py-2">Date</th>
                    <th class="px-4 py-2">Branch</th>
                    <th class="px-4 py-2">Intern</th>
                    <th class="px-4 py-2">Status</th>
                    <th class="px-4 py-2">Description</th>
                    <th class="px-4 py-2">Latitude</th>
                    <th class="px-4 py-2">Longitude</th>
                    <th class="px-4 py-2">Picture</th>
                </tr>
            </thead>
            <tbody class="text-gray-700">
                @foreach($dataAttendances as $attendances)
                <tr>
                    <td class="border px-4 py-2">{{ $attendances->date }}</td>
                    <td class="border px-4 py-2">{{ $attendances->branch_name }}</td>
                    <td class="border px-4 py-2">{{ $attendances->intern_name }}</td>
                    <td class="border px-4 py-2">{{ $attendances->status }}</td>
                    <td class="border px-4 py-2">{{ $attendances->description }}</td>
                    <td class="border px-4 py-2">{{ $attendances->latitude }}</td>
                    <td class="border px-4 py-2">{{ $attendances->longitude }}</td>
                    <td class="border px-4 py-2">
                        <a href="{{ asset('storage/attendance') }}/{{ $attendances->picture }}" target="_blank" class="text-blue-500">See Picture</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</main>

@script
<script>
    $(document).ready(function() {
        var table = $('#datatable').DataTable({
            "lengthMenu": [[5, 10, 20, 50], [5, 10, 20, 50]],
            "pageLength": 10,
            "searching": true,
            "paging": true,
            "pagingType": "simple_numbers",
            "order": [[0, 'desc']],
        });

        $('#branch').on('change', function() {
            var branch = $(this).val().toLowerCase();
            if (branch === "all") {
                table.column(1).search('').draw();
            } else {
                table.column(1).search(branch).draw();
            }
        });
    });
</script>
@endscript