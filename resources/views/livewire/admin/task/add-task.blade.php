<main class="flex-1 p-6">
    <h1 class="text-3xl font-semibold mb-6">Add Task</h1>
    <form wire:submit.prevent="addTask">
        <div class="mb-4">
            <label for="intern" class="block text-gray-700 font-semibold mb-2">Intern</label>
            <select wire:model.lazy="intern" id="intern" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" required>
                <option value="null" selected disabled>Select Intern</option>
                @foreach ($dataInterns as $interns)
                <option value="{{ $interns->id }}">{{ $interns->name }}</option>
                @endforeach
            </select>
            @error('intern') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        <div class="mb-4">
            <label for="title" class="block text-gray-700 font-semibold mb-2">Title</label>
            <input wire:model.lazy="title" type="text" id="title" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" placeholder="Enter title">
            @error('title') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        <div class="mb-4">
            <label for="description" class="block text-gray-700 font-semibold mb-2">Description</label>
            <textarea wire:model.lazy="description" rows="4" id="description" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" placeholder="Enter description"></textarea>
            @error('description') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        <div class="mb-4">
            <label for="deadline" class="block text-gray-700 font-semibold mb-2">Deadline</label>
            <input wire:model.lazy="deadline" type="date" id="deadline" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" placeholder="Enter deadline">
            @error('deadline') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        <div>
            <button type="submit" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-lg">Submit</button>
        </div>
    </form>
</main>
