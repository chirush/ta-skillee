<main class="flex-1 p-6">
    <h1 class="text-3xl font-semibold mb-6">Add Job</h1>
    <form wire:submit.prevent="addJob">
        <div class="mb-4">
            <label for="level" class="block text-gray-700 font-semibold mb-2">Level</label>
            <div>
                <label class="inline-flex items-center">
                    <input type="radio" wire:model.lazy="level" value="High School" class="form-radio text-blue-500">
                    <span class="ml-2">High School</span>
                </label>
            </div>
            <div>
                <label class="inline-flex items-center">
                    <input type="radio" wire:model.lazy="level" value="University" class="form-radio text-blue-500">
                    <span class="ml-2">University</span>
                </label>
            </div>
            @error('level') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        @if (Auth::user()->role == "Superadmin")
        <div class="mb-4">
            <label for="branch_id" class="block text-gray-700 font-semibold mb-2">Branch</label>
            <select wire:model.lazy="branch_id" id="branch_id" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500">
                <option  value="null" selected disabled>Select Branch</option>
                @foreach($dataBranches as $branch)
                    <option value="{{ $branch->id }}">{{ $branch->name }}</option>
                @endforeach
            </select>
            @error('branch_id') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        @endif
        <div class="mb-4">
            <label for="name" class="block text-gray-700 font-semibold mb-2">Position</label>
            <input wire:model.lazy="name" type="text" id="name" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" placeholder="Enter position">
            @error('name') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        <div class="mb-4">
            <label for="description" class="block text-gray-700 font-semibold mb-2">Description</label>
            <textarea wire:model.lazy="description" id="description" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" rows="4" placeholder="Enter description"></textarea>
            @error('description') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        <div class="mb-4">
            <label for="quota" class="block text-gray-700 font-semibold mb-2">Quota</label>
            <input wire:model.lazy="quota" type="number" id="quota" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" placeholder="Enter quota">
            @error('quota') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        <div class="mb-4">
            <label for="date_start" class="block text-gray-700 font-semibold mb-2">Date Start</label>
            <input wire:model.lazy="date_start" type="date" id="date_start" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500">
            @error('date_start') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        <div class="mb-4">
            <label for="deadline" class="block text-gray-700 font-semibold mb-2">Deadline</label>
            <input wire:model.lazy="deadline" type="date" id="deadline" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500">
            @error('deadline') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        <div class="mb-4">
            <label for="periode_start" class="block text-gray-700 font-semibold mb-2">Periode Start</label>
            <input wire:model.lazy="periode_start" type="date" id="periode_start" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500">
            @error('periode_start') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        <div class="mb-4">
            <label for="periode_end" class="block text-gray-700 font-semibold mb-2">Periode End</label>
            <input wire:model.lazy="periode_end" type="date" id="periode_end" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500">
            @error('periode_end') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        <div class="mb-4">
            <label for="criteria" class="block text-gray-700 font-semibold mb-2">Criteria</label>
            <textarea wire:model.lazy="criteria" id="criteria" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" rows="4" placeholder="Enter criteria"></textarea>
            @error('criteria') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        <div>
            <button type="submit" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-lg">Submit</button>
        </div>
    </form>
</main>
