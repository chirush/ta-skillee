<main class="flex-1 p-6">
    <h1 class="text-3xl font-semibold mb-6">Jobs</h1>
    <div class="mb-6">
        <a href="{{ url('/admin/jobs/add') }}" wire:navigate class="bg-gray-800 hover:bg-gray-900 text-white py-2 px-4 rounded-lg">+ Add Job</a>
    </div>
    <div class="mb-2 text-right">
        @php
        $allBranch = getAllBranch();
        @endphp
        <label for="branch" class="mr-2">Filter by Branch:</label>
        <select id="branch" class="bg-white border border-gray-300 rounded px-2 py-1 text-sm">
            <option value="all">All</option>
            @foreach ($allBranch as $branches)
            <option value="{{ $branches->name }}">{{ $branches->name }}</option>
            @endforeach
        </select>
    </div>
    <div class="overflow-x-auto">
        <table id="datatable" class="table-auto min-w-full bg-white shadow-md rounded-lg overflow-hidden">
            <thead class="bg-gray-800 text-white">
                <tr>
                    <th class="px-4 py-2">ID</th>
                    <th class="px-4 py-2">Branch</th>
                    <th class="px-4 py-2">Position</th>
                    <th class="px-4 py-2">Description</th>
                    <th class="px-4 py-2">Quota</th>
                    <th class="px-4 py-2">Date Start</th>
                    <th class="px-4 py-2">Deadline</th>
                    <th class="px-4 py-2">Level</th>
                    <th class="px-4 py-2">Periode</th>
                    <th class="px-4 py-2">Criteria</th>
                    <th class="px-4 py-2"></th>
                </tr>
            </thead>
            <tbody class="text-gray-700">
                @foreach($dataJobs as $jobs)
                <tr>
                    <td class="border px-4 py-2">{{ $jobs->id }}</td>
                    <td class="border px-4 py-2">{{ $jobs->branch_name }}</td>
                    <td class="border px-4 py-2">{{ $jobs->name }}</td>
                    <td class="border px-4 py-2">{{ $jobs->description }}</td>
                    <td class="border px-4 py-2">{{ $jobs->quota }}</td>
                    <td class="border px-4 py-2">{{ $jobs->date_start }}</td>
                    <td class="border px-4 py-2">{{ $jobs->deadline }}</td>
                    <td class="border px-4 py-2">{{ $jobs->level }}</td>
                    <td class="border px-4 py-2">{{ $jobs->periode_start }} - {{ $jobs->periode_end }}</td>
                    <td class="border px-4 py-2">{{ $jobs->criteria }}</td>
                    <td class="border px-4 py-2">
                        <a href="{{ url('/admin/jobs/edit/') }}/{{ $jobs->id }}" wire:navigate class="text-blue-700 hover:text-blue-900 mr-2">Edit</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</main>

@script
<script>
    $(document).ready(function() {
        var table = $('#datatable').DataTable({
            "lengthMenu": [[5, 10, 20, 50], [5, 10, 20, 50]],
            "pageLength": 10,
            "searching": true,
            "paging": true,
            "pagingType": "simple_numbers",
            "order": [[0, 'desc']],
        });

        $('#branch').on('change', function() {
            var branch = $(this).val().toLowerCase();
            if (branch === "all") {
                table.column(1).search('').draw();
            } else {
                table.column(1).search(branch).draw();
            }
        });
    });
</script>
@endscript