<main class="flex-1 p-6">
    <h1 class="text-3xl font-semibold mb-6">Applications</h1>
    <div class="mb-2 text-right">
        @php
        $allBranch = getAllBranch();
        @endphp
        <label for="branch" class="mr-2">Filter by Branch:</label>
        <select id="branch" class="bg-white border border-gray-300 rounded px-2 py-1 text-sm">
            <option value="all">All</option>
            @foreach ($allBranch as $branches)
            <option value="{{ $branches->name }}">{{ $branches->name }}</option>
            @endforeach
        </select>
        <label for="status" class="mr-2">Filter by Status:</label>
        <select id="status" class="bg-white border border-gray-300 rounded px-2 py-1 text-sm">
            <option value="all">All</option>
            <option value="Pending">Pending</option>
            <option value="Approved">Approved</option>
            <option value="Declined">Declined</option>
        </select>
    </div>
    <div class="overflow-x-auto">
        <table id="datatable" class="table-auto min-w-full bg-white shadow-md rounded-lg overflow-hidden">
            <thead class="bg-gray-800 text-white">
                <tr>
                    <th class="px-4 py-2">ID</th>
                    <th class="px-4 py-2">Job</th>
                    <th class="px-4 py-2">Branch</th>
                    <th class="px-4 py-2">Intern</th>
                    <th class="px-4 py-2">Resume</th>
                    <th class="px-4 py-2">Application Date</th>
                    <th class="px-4 py-2">Status</th>
                    <th class="px-4 py-2"></th>
                </tr>
            </thead>
            <tbody class="text-gray-700">
                @foreach($dataApplications as $application)
                <tr>
                    <td class="border px-4 py-2">{{ $application->id }}</td>
                    <td class="border px-4 py-2">{{ $application->job_name }}</td>
                    <td class="border px-4 py-2">{{ $application->branch_name }}</td>
                    <td class="border px-4 py-2">{{ $application->intern_name }}</td>
                    <td class="border px-4 py-2"><a href="{{ asset('storage/resumes') }}/{{ $application->resume }}" target="_blank">View Resume</a></td>
                    <td class="border px-4 py-2">{{ $application->application_date }}</td>
                    <td class="border px-4 py-2">{{ $application->status }}</td>
                    <td class="border px-4 py-2">
                        <a href="{{ url('/admin/applications/update/') }}/{{ $application->id }}" wire:navigate class="text-blue-700 hover:text-blue-900 mr-2">Update</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</main>


@script
<script>
    $(document).ready(function() {
        var table = $('#datatable').DataTable({
            "lengthMenu": [[5, 10, 20, 50], [5, 10, 20, 50]],
            "pageLength": 10,
            "searching": true,
            "paging": true,
            "pagingType": "simple_numbers",
            "order": [[0, 'desc']],
        });

        $('#branch').on('change', function() {
            var branch = $(this).val().toLowerCase();
            if (branch === "all") {
                table.column(2).search('').draw();
            } else {
                table.column(2).search(branch).draw();
            }
        });
    });
</script>
@endscript