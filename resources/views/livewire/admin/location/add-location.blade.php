<main class="flex-1 p-6">
    <h1 class="text-3xl font-semibold mb-6">Add Location</h1>
    <form wire:submit.prevent="addLocation">
        <div class="mb-4">
            <label for="latitude" class="block text-gray-700 font-semibold mb-2">Latitude</label>
            <input wire:model.lazy="latitude" type="text" id="latitude" latitude="latitude" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" placeholder="Enter latitude">
            @error('latitude') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        <div class="mb-4">
            <label for="longitude" class="block text-gray-700 font-semibold mb-2">Longitude</label>
            <textarea wire:model.lazy="longitude" id="longitude" latitude="longitude" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" placeholder="Enter longitude"></textarea>
            @error('longitude') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        <div>
            <button type="submit" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-lg">Submit</button>
        </div>
    </form>
</main>
