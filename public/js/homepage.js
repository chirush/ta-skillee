document.getElementById("menu-toggle").addEventListener("click", function() {
    document.getElementById("mobile-menu").classList.toggle("hidden");
});

document.addEventListener('DOMContentLoaded', function () {
    var splide = new Splide('#image-carousel', {
        type   : 'loop',
        perPage: 1,
        autoplay: true,
        autoplayOptions: {
            delay: 500,
        },
        arrows : false,
    });

    splide.on('mounted move', function () {
        var activeSlide = splide.Components.Elements.slides[splide.index];
        activeSlide.classList.add('animate__animated', 'animate__fadeInRight');
    });

    splide.mount();
});

document.addEventListener('DOMContentLoaded', function () {
    const observer = new IntersectionObserver((entries) => {
        entries.forEach(entry => {
            if (entry.isIntersecting) {
                entry.target.classList.add('animate__fadeIn');
                entry.target.style.animationDelay = entry.target.getAttribute('data-delay') || '0s';
                observer.unobserve(entry.target);
            }
        });
    }, { threshold: 0.1 });

    const elementsToAnimate = document.querySelectorAll('.animate__animated');
    elementsToAnimate.forEach((el, index) => {
        el.setAttribute('data-delay', `${index * 0.3 + 0.3}s`);
        observer.observe(el);
    });
});

function scrollToTarget(event) {
    event.preventDefault();
    const targetId = event.target.getAttribute('href').substring(1);
    document.getElementById(targetId).scrollIntoView({
        behavior: 'smooth'
    });
}