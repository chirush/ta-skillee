document.addEventListener("DOMContentLoaded", function() {
    let map;
    let marker, circle;
    let currentLatitude, currentLongitude;
    const specificLocationLat = window.specificLocationLat;
    const specificLocationLon = window.specificLocationLon;
    const radius = 0.05;

    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition, showError);
        } else {
            alert('Perangkat Anda tidak mendukung geolokasi.');
        }
    }

    function showPosition(data) {
        currentLatitude = data.coords.latitude;
        currentLongitude = data.coords.longitude;

        document.getElementById('latitude').value = currentLatitude;
        document.getElementById('longitude').value = currentLongitude;
        showMap(currentLatitude, currentLongitude);
    }

    function showError(error) {
        let error_message = '';
        switch (error.code) {
            case error.PERMISSION_DENIED:
                error_message = "Pengguna menolak permintaan Geolocation.";
                break;
            case error.POSITION_UNAVAILABLE:
                error_message = "Informasi lokasi tidak tersedia.";
                break;
            case error.TIMEOUT:
                error_message = "Permintaan untuk mendapatkan lokasi pengguna melewati batas waktu.";
                break;
            case error.UNKNOWN_ERROR:
                error_message = "Terjadi kesalahan yang tidak diketahui.";
                break;
        }
        alert(error_message);
    }

    function calculateDistance(lat1, lon1, lat2, lon2) {
        const R = 6371;
        const dLat = (lat2 - lat1) * Math.PI / 180;
        const dLon = (lon2 - lon1) * Math.PI / 180;
        const a =
            Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
            Math.sin(dLon / 2) * Math.sin(dLon / 2);
        const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        const distance = R * c;
        return distance;
    }

    function showMap(latitude, longitude) {
        if (!map) {
            map = L.map("mapid").setView([latitude, longitude], 18);
            L.tileLayer(
                "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
                    maxZoom: 18,
                    attribution: 'Peta data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> kontribusi, ' +
                        '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
                        'Gambar © <a href="https://www.mapbox.com/">Mapbox</a>',
                    id: "mapbox/streets-v11",
                    tileSize: 512,
                    zoomOffset: -1,
                }
            ).addTo(map);
        } else {
            map.setView([latitude, longitude], 18);
        }

        if (marker) {
            map.removeLayer(marker);
        }
        marker = L.marker([latitude, longitude])
            .addTo(map)
            .bindPopup("Lokasi Anda");

        if (circle) {
            map.removeLayer(circle);
        }
        circle = L.circle([specificLocationLat, specificLocationLon], {
            color: 'blue',
            fillColor: '#4285f4',
            fillOpacity: 0.5,
            radius: radius * 1000
        }).addTo(map);
    }

    window.refreshLocation = function() {
        getLocation();
    }

    getLocation();

    document.querySelector("form").addEventListener("submit", function(event) {
        var status = document.getElementById('status').value;
        const distance = calculateDistance(currentLatitude, currentLongitude, specificLocationLat, specificLocationLon);

        if (distance > radius && status === 'Hadir') {
            event.preventDefault();
            alert("Anda tidak berada dalam radius yang diizinkan. Pastikan Anda berada dalam lokasi yang ditentukan.");
        }
    });
});

document.addEventListener("DOMContentLoaded", function() {
    function toggleAlasanField() {
        var status = document.getElementById('status').value;
        var alasanField = document.getElementById('alasanField');
        if (status === 'Izin' || status === 'Sakit') {
            alasanField.style.display = 'block';
        } else {
            alasanField.style.display = 'none';
        }
    }

    document.getElementById('status').addEventListener('change', toggleAlasanField);
    
    toggleAlasanField();
});
