<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->id();
            $table->foreignId('intern_id')->unsigned()->references('id')->on('interns')->onDelete('cascade');
            $table->foreignId('job_id')->unsigned()->references('id')->on('jobs')->onDelete('cascade');
            $table->string('resume');
            $table->string('application_date');
            $table->enum('status', ['Pending', 'Approved', 'Declined']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('applications');
    }
};
