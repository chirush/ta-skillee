<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class VerificationEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $intern;

    public function __construct($intern)
    {
        $this->intern = $intern;
    }

    public function build()
    {
        return $this->markdown('emails.verification.verify')
            ->subject('Verify Your Email Address');
    }
}