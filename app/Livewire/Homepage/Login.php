<?php

namespace App\Livewire\Homepage;

use Livewire\Component;
use Session;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\Intern;
use Illuminate\Support\Facades\Hash;

class Login extends Component
{
    public $email;
    public $password;
    public $loginError;
    public $verificationError;
    public $captcha = null;
     
    public function updatedCaptcha($token)
    {
        $response = Http::post(
            'https://www.google.com/recaptcha/api/siteverify?secret='.
            env('CAPTCHA_SECRET_KEY').
            '&response='.$token
        );
     
        $success = $response->json()['success'];
     
        if (! $success) {
            throw ValidationException::withMessages([
                'captcha' => __('Google thinks, you are a bot, please refresh and try again!'),
            ]);
        } else {
            $this->captcha = true;
        }
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'email' => 'required|email',
            'password' => 'required',
            'captcha' => 'required',
        ]);
    }

    public function processLogin()
    {
        $validatedData = $this->validate([
            'email' => 'required|email',
            'password' => 'required',
            'captcha' => 'required',
        ]);

        if (Auth::attempt(['email' => $this->email, 'password' => $this->password])) {
            $user = Auth::user();

            return redirect('/admin/messages');
        }

        $intern = Intern::where('email', $this->email)->first();
        if ($intern && Hash::check($this->password, $intern->password)) {
            $verify = $intern->email_verified_at;

            if ($verify != "") {
                Auth::guard('intern')->login($intern);
                
                if ($intern->branch_id && $intern->date_start <= now()){
                    return redirect('/internship/attendances');
                }
                else{
                    return redirect('/internship/jobs');
                }
            } else {
                $this->verificationError = 'Akun belum di verifikasi';
            }
        }

        $this->loginError = 'Username atau Password salah!';
    }

    public function mount()
    {
        if (Auth::guard('intern')->user()) {
            return redirect('/internship/jobs');
        }

        if (Auth::user()) {
            return redirect('/admin/messages');
        }
    }

    public function render()
    {
        return view('livewire.homepage.login')->layout('livewire.layout.authentication');
    }
}