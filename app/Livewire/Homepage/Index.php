<?php

namespace App\Livewire\Homepage;

use Livewire\Component;
use App\Models\Message;
use Illuminate\Support\Facades\Cache;

class Index extends Component
{
    public $name;
    public $email;
    public $message;

    protected $rules = [
        'name' => 'required',
        'email' => 'required|email',
        'message' => 'required',
    ];

    public function sendMessage()
    {
        $this->validate();

        $ipAddress = request()->ip();
        $cacheKey = 'contact_form_' . $ipAddress;

        if (Cache::has($cacheKey)) {
            session()->flash('error', 'You must wait 5 minutes before submitting again.');
            return;
        }

        Message::create([
            'name' => $this->name,
            'email' => $this->email,
            'message' => $this->message,
            'status' => "Unread"
        ]);

        Cache::put($cacheKey, true, now()->addMinutes(5));

        session()->flash('success', 'Message sent successfully!');

        $this->reset(['name', 'email', 'message']);
    }

    public function render()
    {
        return view('livewire.homepage.index')->layout('livewire.layout.homepage');
    }
}
