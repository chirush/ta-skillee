<?php

namespace App\Livewire\Homepage;

use Livewire\Component;
use Illuminate\Support\Str;
use App\Models\Instance;
use App\Models\Intern;
use App\Models\InstanceFaculty;
use App\Models\InstanceProgram;
use App\Models\Province;
use App\Models\Regency;
use App\Models\District;
use Mail;
use App\Mail\VerificationEmail;

class Register extends Component
{
    public $name;
    public $email;
    public $password;
    public $password_confirmation;
    public $phone;
    public $nik;
    public $domicile;
    public $instance;
    public $faculty;
    public $program;
    public $province;
    public $regency;
    public $district;
    public $dataInstance;
    public $dataProvinces;
    public $programType;

    public function updated($field)
    {
        $this->validateOnly($field, [
            'name' => 'required',
            'email' => 'required|email|unique:interns,email',
            'password' => 'required|confirmed',
            'phone' => 'required|unique:interns,phone',
            'nik' => 'required|unique:interns,nik',
            'domicile' => 'required',
            'instance' => 'required',
            'province' => 'required',
            'regency' => 'required',
            'district' => 'required',
        ]);
    }

    public function mount()
    {
        $level = request()->query('program');

        if ($level == "high-school") {
            $this->programType = "High School";
        } else {
            $this->programType = "University";
        }

        $this->dataInstance = Instance::where('level', $this->programType)->get();
        $this->dataProvinces = Province::all();
    }

    public function fetchInstances()
    {
        $this->dataInstance = Instance::where('level', $this->programType)->get();
    }

    public function fetchRegency()
    {
        $regencies = Regency::where('province_id', $this->province)->get();

        return $regencies;
    }

    public function fetchDistrict()
    {
        $districts = District::where('regency_id', $this->regency)->get();

        return $districts;
    }

    public function processRegistration()
    {
        $validatedData = $this->validate([
            'name' => 'required',
            'email' => 'required|email|unique:interns,email',
            'password' => 'required|confirmed',
            'phone' => 'required|unique:interns,phone',
            'nik' => 'required|unique:interns,nik',
            'instance' => 'required',
            'province' => 'required',
            'regency' => 'required',
            'district' => 'required',
        ]);

        $verification_token = Str::random(40);

        $domicile = $this->province . "," . $this->regency . "," . $this->district;

        $intern = Intern::create([
            'name' => $this->name,
            'email' => $this->email,
            'password' => bcrypt($this->password),
            'phone' => $this->phone,
            'nik' => $this->nik,
            'domicile' => $domicile,
            'instance_id' => $this->instance,
            'faculty' => $this->faculty,
            'program' => $this->program,
            'verification_token' => $verification_token,
            'level' => $this->programType,
        ]);

        Mail::to($this->email)->send(new VerificationEmail($intern));

        session()->flash('verificationMessage', 'Pendaftaran berhasil! Silahkan cek email untuk verifikasi akun anda.');

        return redirect('/login');
    }

    public function render()
    {
        return view('livewire.homepage.register', [
            'dataInstance' => $this->dataInstance,
            'instances' => $this->fetchInstances(),
            'regencies' => $this->fetchRegency(),
            'districts' => $this->fetchDistrict(),
        ])->layout('livewire.layout.authentication');
    }
}

