<?php

namespace App\Livewire\Intern\Task;

use Livewire\Component;
use App\Models\Task;
use App\Models\Branch;

class UpdateTask extends Component
{
    public $taskId;
    public $title;
    public $description;
    public $report;
    public $reviseNote;

    public function mount($id)
    {
        $this->taskId = $id;
        $task = Task::findOrFail($id);
        $this->title = $task->title;
        $this->description = $task->description;
        $this->report = $task->report;
        $this->reviseNote = $task->reviseNote;
    }

    public function render()
    {
        return view('livewire.intern.task.update-task', [
            'taskId' => $this->taskId,
        ])->layout('livewire.layout.internship');
    }
}
