<?php

namespace App\Livewire\Intern\Task;

use Livewire\Component;
use App\Models\Task as Tasks;
use Illuminate\Support\Facades\Auth;

class Task extends Component
{
    public $dataTasks;

    public function mount()
    {
        if (!Auth::guard('intern')->user()->branch_id) {
            return redirect('internship/jobs');
        }

        $this->dataTasks = Tasks::all();
    }

    public function render()
    {
        return view('livewire.intern.task.task', [
            'dataTasks' => $this->dataTasks,
        ])->layout('livewire.layout.internship');
    }
}
