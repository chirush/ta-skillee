<?php

namespace App\Livewire\Intern\Report;

use Livewire\Component;
use App\Models\Report as Reports;
use Illuminate\Support\Facades\Auth;

class Report extends Component
{
    public $dataReports;

    public function mount()
    {
        if (!Auth::guard('intern')->user()->branch_id) {
            return redirect('internship/jobs');
        }

        $this->dataReports = Reports::all();
    }

    public function deleteReport($reportId)
    {
        $report = Reports::findOrFail($reportId);
        $report->delete();

        $this->dataReports = Reports::all();
    }

    public function render()
    {
        return view('livewire.intern.report.report', [
            'dataReports' => $this->dataReports,
        ])->layout('livewire.layout.internship');
    }
}
