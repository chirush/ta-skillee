<?php

namespace App\Livewire\Intern\Report;

use Livewire\Component;
use App\Models\Report;
use Illuminate\Support\Facades\Auth;

class AddReport extends Component
{
    public $title;
    public $description;
    public $date;

    public function updated($field)
    {
        $this->validateOnly($field, [
            'title' => 'required',
            'description' => 'required',
            'date' => 'required',
        ]);
    }

    public function addReport()
    {
        $validatedData = $this->validate([
            'title' => 'required',
            'description' => 'required',
            'date' => 'required',
        ]);

        $dateTime = strtotime($this->date);
        $date = date('Y-m-d', $dateTime);

        $internId = Auth::guard('intern')->user()->id;

        $report = Report::create([
            'intern_id' => $internId,
            'title' => $this->title,
            'description' => $this->description,
            'date' => $date,
        ]);

        return redirect('/internship/reports');
    }

    public function mount()
    {
        if (!Auth::guard('intern')->user()->branch_id) {
            return redirect('internship/jobs');
        }
        
        $this->date = now()->format('Y-m-d');
    }

    public function render()
    {
        return view('livewire.intern.report.add-report')->layout('livewire.layout.internship');
    }
}
