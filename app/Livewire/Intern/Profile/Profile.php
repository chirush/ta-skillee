<?php

namespace App\Livewire\Intern\Profile;

use Livewire\Component;
use App\Models\Intern;
use App\Models\Instance;
use App\Models\Province;
use App\Models\Regency;
use App\Models\District;
use Illuminate\Support\Facades\Auth;

class Profile extends Component
{
    public $name;
    public $email;
    public $phone;
    public $nik;
    public $gender;
    public $domicile;
    public $mentor_name;
    public $mentor_email;
    public $mentor_phone;
    public $instance;
    public $faculty;
    public $program;
    public $formattedDomicile;
    public $bio;

    public function mount()
    {
        $this->name = Auth::guard('intern')->user()->name;
        $this->email = Auth::guard('intern')->user()->email;
        $this->phone = Auth::guard('intern')->user()->phone;
        $this->bio = Auth::guard('intern')->user()->bio;
        $this->nik = Auth::guard('intern')->user()->nik;
        $this->domicile = Auth::guard('intern')->user()->domicile;
        $this->mentor_name = Auth::guard('intern')->user()->mentor_name;
        $this->mentor_email = Auth::guard('intern')->user()->mentor_email;
        $this->mentor_phone = Auth::guard('intern')->user()->mentor_phone;
        $this->faculty = Auth::guard('intern')->user()->faculty;
        $this->program = Auth::guard('intern')->user()->program;
        $this->instance = Instance::where('id', Auth::guard('intern')->user()->instance_id)->value('name');

        list($provinceId, $regencyId, $districtId) = explode(',', $this->domicile);

        $province = ucwords(strtolower(Province::find($provinceId)->name));
        $regency = ucwords(strtolower(Regency::find($regencyId)->name));
        $district = ucwords(strtolower(District::find($districtId)->name));

        $this->formattedDomicile = $province . ", " . $regency . ", " . $district;
    }

    public function updateYourContact()
    {
        $validatedData = $this->validate([
            'name' => 'required',
            'phone' => 'required',
        ]);

        $user = Auth::guard('intern')->user();
        $user->name = $this->name;
        $user->phone = $this->phone;
        $user->save();
    }

    public function updateMentorContact()
    {
        $validatedData = $this->validate([
            'mentor_name' => 'required',
            'mentor_email' => 'required',
            'mentor_phone' => 'required',
        ]);

        $user = Auth::guard('intern')->user();
        $user->mentor_name = $this->mentor_name;
        $user->mentor_email = $this->mentor_email;
        $user->mentor_phone = $this->mentor_phone;
        $user->save();
    }

    public function updatePersonalInfo()
    {
        $validatedData = $this->validate([
            'nik' => 'required',
            'gender' => 'required',
        ]);

        $user = Auth::guard('intern')->user();
        $user->nik = $this->nik;
        $user->gender = $this->gender;
        $user->save();
    }

    public function updateEducation()
    {
        $validatedData = $this->validate([
            'faculty' => 'required',
            'program' => 'required',
        ]);

        $user = Auth::guard('intern')->user();
        $user->faculty = $this->faculty;
        $user->program = $this->program;
        $user->save();
    }

    public function updateBio()
    {
        $validatedData = $this->validate([
            'bio' => 'required',
        ]);

        $user = Auth::guard('intern')->user();
        $user->bio = $this->bio;
        $user->save();
    }

    public function render()
    {
        return view('livewire.intern.profile.profile')->layout('livewire.layout.internship');
    }
}