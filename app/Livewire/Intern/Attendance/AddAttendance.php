<?php

namespace App\Livewire\Intern\Attendance;

use Livewire\Component;
use App\Models\Attendance;
use App\Models\Branch;
use Illuminate\Support\Facades\Auth;

class AddAttendance extends Component
{
    public $date;
    public $specificLocationLat;
    public $specificLocationLon;

    public function mount()
    {
        if (!Auth::guard('intern')->user()->branch_id) {
            return redirect('internship/jobs');
        }
        
        $this->date = now()->format('Y-m-d');

        $branchId = Auth::guard('intern')->user()->branch_id;

        $branch = Branch::where('id', $branchId)->first();

        $this->specificLocationLat = $branch->latitude;
        $this->specificLocationLon = $branch->longitude;
    }

    public function render()
    {
        return view('livewire.intern.attendance.add-attendance')
            ->layout('livewire.layout.internship');
    }
}
