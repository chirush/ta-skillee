<?php

namespace App\Livewire\Admin\Job;

use Livewire\Component;
use App\Models\Jobs;
use App\Models\Branch;
use Carbon\Carbon;

class EditJob extends Component
{
    public $branches;
    public $name;
    public $description;
    public $quota;
    public $date_start;
    public $deadline;
    public $level;
    public $jobId;
    public $branch_id;
    public $periode_start;
    public $periode_end;
    public $criteria;

    public function mount($id)
    {
        $this->jobId = $id;
        $job = Jobs::findOrFail($id);
        $this->branch_id = $job->branch_id;
        $this->name = $job->name;
        $this->description = $job->description;
        $this->date_start = $job->date_start;
        $this->deadline = $job->deadline;
        $this->quota = $job->quota;
        $this->level = $job->level;
        $this->periode_start = $job->periode_start;
        $this->periode_end = $job->periode_end;
        $this->criteria = $job->criteria;

        $this->branches = Branch::all();
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'branch_id' => 'required',
            'name' => 'required',
            'description' => 'required',
            'quota' => 'required',
            'date_start' => 'required|date',
            'deadline' => 'required|date',
            'periode_start' => 'required|date',
            'periode_end' => 'required|date',
            'criteria' => 'required',
            'level' => 'required',
        ]);
    }

    public function editJob()
    {
        $dbJob = Jobs::findOrFail($this->jobId);

        $validatedData = $this->validate([
            'branch_id' => 'required',
            'name' => 'required',
            'description' => 'required',
            'quota' => 'required',
            'date_start' => 'required|date',
            'deadline' => 'required|date',
            'periode_start' => 'required|date',
            'periode_end' => 'required|date',
            'criteria' => 'required',
            'level' => 'required',
        ]);

        $job = [
            'branch_id' => $this->branch_id,
            'name' => $this->name,
            'description' => $this->description,
            'quota' => $this->quota,
            'date_start' => $this->date_start,
            'deadline' => $this->deadline,
            'periode_start' => $this->periode_start,
            'periode_end' => $this->periode_end,
            'criteria' => $this->criteria,
            'level' => $this->level,
        ];

        $dbJob->update($job);

        return redirect('/admin/jobs');
    }

    public function render()
    {
        return view('livewire.admin.job.edit-job', [
            'dataBranches' => $this->branches,
        ])->layout('livewire.layout.admin');
    }
}
