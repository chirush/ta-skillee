<?php

namespace App\Livewire\Admin\Job;

use Livewire\Component;
use App\Models\Jobs;
use App\Models\Branch;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class AddJob extends Component
{
    public $branch_id;
    public $name;
    public $description;
    public $quota;
    public $date_start;
    public $deadline;
    public $level;
    public $periode_start;
    public $periode_end;
    public $criteria;
    public $branches;

    public function mount()
    {
        $this->branches = Branch::all();
    }

    public function addJob()
    {
        if (Auth::user()->role == "Human Resources"){
            $branch_id = Auth::user()->branch_id;
        }else{
            $branch_id = $this->branch;
        }
        
        $validatedData = $this->validate([
            'name' => 'required',
            'description' => 'required',
            'quota' => 'required',
            'date_start' => 'required',
            'deadline' => 'required',
            'level' => 'required',
            'periode_start' => 'required',
            'periode_end' => 'required',
            'criteria' => 'required',
        ]);

        $location = Jobs::create([
            'branch_id' => $branch_id,
            'name' => $this->name,
            'description' => $this->description,
            'quota' => $this->quota,
            'date_start' => $this->date_start,
            'deadline' => $this->deadline,
            'level' => $this->level,
            'applicator' => 0,
            'periode_start' => $this->periode_start,
            'periode_end' => $this->periode_end,
            'criteria' => $this->criteria,
        ]);

        return redirect('/admin/jobs');
    }

    public function render()
    {
        return view('livewire.admin.job.add-job', [
            'dataBranches' => $this->branches,
        ])->layout('livewire.layout.admin');
    }
}
