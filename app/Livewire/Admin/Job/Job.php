<?php

namespace App\Livewire\Admin\Job;

use Livewire\Component;
use App\Models\Jobs;
use Illuminate\Support\Facades\Auth;

class Job extends Component
{
    public $dataJobs;

    public function mount()
    {
        $user = Auth::user();
        $query = Jobs::join('branches', 'jobs.branch_id', '=', 'branches.id')
            ->select('jobs.*', 'branches.name as branch_name');

        if ($user->role == "Human Resources") {
            $query->where('jobs.branch_id', $user->branch_id);
        }

        $this->dataJobs = $query->get();
    }

    public function deleteJob($id)
    {
        Jobs::find($id)->delete();
        $this->loadJobs();
    }

    public function render()
    {
        return view('livewire.admin.job.job', [
            'dataJobs' => $this->dataJobs,
        ])->layout('livewire.layout.admin');
    }
}
