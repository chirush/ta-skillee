<?php

namespace App\Livewire\Admin\Location;

use Livewire\Component;
use App\Models\Location as Locations;

class EditLocation extends Component
{
    public $latitude;
    public $longitude;
    public $locationId;

    public function updated($field)
    {
        $this->validateOnly($field, [
            'latitude' => 'required',
            'longitude' => 'required',
        ]);
    }

    public function mount($id)
    {
        $this->locationId = $id;
        $location = Locations::findOrFail($id);
        $this->latitude = $location->latitude;
        $this->longitude = $location->longitude;
    }

    public function editLocation()
    {
        $dbLocation = Locations::findOrFail($this->locationId);

        $validatedData = $this->validate([
            'latitude' => 'required',
            'longitude' => 'required',
        ]);

        $location = [
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
        ];

        $dbLocation->update($location);

        return redirect('/admin/locations');
    }

    public function render()
    {
        return view('livewire.admin.location.edit-location')->layout('livewire.layout.admin');
    }
}
