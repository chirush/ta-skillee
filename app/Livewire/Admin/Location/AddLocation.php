<?php

namespace App\Livewire\Admin\Location;

use Livewire\Component;
use App\Models\Location;

class AddLocation extends Component
{
    public $latitude;
    public $longitude;

    public function updated($field)
    {
        $this->validateOnly($field, [
            'latitude' => 'required',
            'longitude' => 'required',
        ]);
    }

    public function addLocation()
    {
        $validatedData = $this->validate([
            'latitude' => 'required',
            'longitude' => 'required',
        ]);

        $location = Location::create([
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
        ]);

        return redirect('/admin/locations');

    }
    public function render()
    {
        return view('livewire.admin.location.add-location')->layout('livewire.layout.admin');
    }
}
