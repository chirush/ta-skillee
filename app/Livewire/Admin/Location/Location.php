<?php

namespace App\Livewire\Admin\Location;

use Livewire\Component;
use App\Models\Location as Locations;

class Location extends Component
{
    public $dataLocations;

    public function mount()
    {
        $this->dataLocations = Locations::all();
    }

    public function deleteLocation($id)
    {
        Locations::find($id)->delete();
        $this->loadLocations(); 
    }

    public function render()
    {
        return view('livewire.admin.location.location', [
            'dataLocations' => $this->dataLocations,
        ])->layout('livewire.layout.admin');
    }
}
