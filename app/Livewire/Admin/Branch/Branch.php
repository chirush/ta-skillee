<?php

namespace App\Livewire\Admin\Branch;

use Livewire\Component;
use App\Models\Branch as Branches;

class Branch extends Component
{
    public $dataBranches;

    public function mount()
    {
        $this->loadBranches();
    }

    public function loadBranches()
    {
        $this->dataBranches = Branches::all();
    }

    public function deleteBranche($id)
    {
        Branches::find($id)->delete();
        $this->loadBranches();
    }

    public function render()
    {
        return view('livewire.admin.branch.branch', [
            'dataBranches' => $this->dataBranches,
        ])->layout('livewire.layout.admin');
    }
}
