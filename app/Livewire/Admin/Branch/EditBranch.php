<?php

namespace App\Livewire\Admin\Branch;

use Livewire\Component;
use App\Models\Branch as Branches;

class EditBranch extends Component
{
    public $branchId;
    public $name;
    public $contact;
    public $address;
    public $latitude;
    public $longitude;

    public function updated($field)
    {
        $this->validateOnly($field, [
            'name' => 'required|unique:branches,name',
            'contact' => 'required',
            'address' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
        ]);
    }

    public function mount($id)
    {
        $this->branchId = $id;
        $branch = Branches::findOrFail($id);
        $this->name = $branch->name;
        $this->contact = $branch->contact;
        $this->address = $branch->address;
        $this->latitude = $branch->latitude;
        $this->longitude = $branch->longitude;
    }

    public function editBranch()
    {
        $dbBranch = Branches::findOrFail($this->branchId);

        $validatedData = $this->validate([
            'name' => 'required|unique:branches,name,' . $dbBranch->id,
            'contact' => 'required',
            'address' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
        ]);

        $branch = [
            'name' => $this->name,
            'contact' => $this->contact,
            'address' => $this->address,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
        ];

        $dbBranch->update($branch);

        return redirect('/admin/branches');
    }

    public function render()
    {
        return view('livewire.admin.branch.edit-branch')->layout('livewire.layout.admin');
    }
}
