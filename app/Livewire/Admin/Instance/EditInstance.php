<?php

namespace App\Livewire\Admin\Instance;

use Livewire\Component;
use App\Models\Instance as Instances;

class EditInstance extends Component
{
    public $name;
    public $description;
    public $level;
    public $instanceId;

    public function updated($field)
    {
        $this->validateOnly($field, [
            'name' => 'required|unique:instances,name',
            'description' => 'required',
            'level' => 'required',
        ]);
    }

    public function mount($id)
    {
        $this->instanceId = $id;
        $instance = Instances::findOrFail($id);
        $this->name = $instance->name;
        $this->description = $instance->description;
        $this->level = $instance->level;
    }

    public function editInstance()
    {
        $dbInstance = Instances::findOrFail($this->instanceId);

        $validatedData = $this->validate([
            'name' => 'required|unique:instances,name,' . $dbInstance->id,
            'description' => 'required',
            'level' => 'required',
        ]);

        $instance = [
            'name' => $this->name,
            'description' => $this->description,
            'level' => $this->level,
        ];

        $dbInstance->update($instance);

        return redirect('/admin/instances');
    }

    public function render()
    {
        return view('livewire.admin.instance.edit-instance')->layout('livewire.layout.admin');
    }
}
