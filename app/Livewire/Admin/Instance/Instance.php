<?php

namespace App\Livewire\Admin\Instance;

use Livewire\Component;
use App\Models\Instance as Instances;

class Instance extends Component
{
    public $dataInstances;

    public function mount()
    {
        $this->loadInstances();
    }

    public function loadInstances()
    {
        $this->dataInstances = Instances::all();
    }

    public function deleteInstance($id)
    {
        Instances::find($id)->delete();
        $this->loadInstances();
    }

    public function render()
    {
        return view('livewire.admin.instance.instance', [
            'dataInstances' => $this->dataInstances,
        ])->layout('livewire.layout.admin');
    }
}
