<?php

namespace App\Livewire\Admin\Monitor;

use Livewire\Component;
use App\Models\Report as Reports;
use Illuminate\Support\Facades\Auth;

class Report extends Component
{
    public $dataReports;

    public function mount()
    {
        $user = Auth::user();
        $query = Reports::join('interns', 'reports.intern_id', '=', 'interns.id')
            ->join('branches', 'interns.branch_id', '=', 'branches.id')
            ->select('reports.*', 'interns.name as intern_name', 'branches.name as branch_name');

        if ($user->role == "Instructor") {
            $query->where('interns.instructor_id', $user->id);
        } elseif ($user->role == "Human Resources") {
            $query->where('interns.branch_id', $user->branch_id);
        }

        $this->dataReports = $query->get();
    }

    public function render()
    {
        return view('livewire.admin.monitor.report', [
            'dataReports' => $this->dataReports,
        ])->layout('livewire.layout.admin');
    }
}
