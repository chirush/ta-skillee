<?php

namespace App\Livewire\Admin\Task;

use Livewire\Component;
use App\Models\Task;
use App\Models\Intern;

class ReviewTask extends Component
{
    public $taskId;
    public $intern_name;
    public $title;
    public $description;
    public $report;
    public $file;
    public $picture;
    public $status;
    public $revise_note;

    public function mount($id)
    {
        $task = Task::findOrFail($id);
        $intern = Intern::findOrFail($task->intern_id);
        $this->intern_name = $intern->name;
        $this->taskId = $task->id;
        $this->title = $task->title;
        $this->description = $task->description;
        $this->report = $task->report;
        $this->file = $task->file;
        $this->picture = $task->picture;
        $this->status = $task->status;
        $this->revise_note = $task->revise_note;
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'status' => 'required',
        ]);
    }

    public function reviewTask()
    {
        $dbTask = Task::findOrFail($this->taskId);

        $validatedData = $this->validate([
            'status' => 'required',
        ]);

        $task = [
            'status' => $this->status,
            'revise_note' => $this->revise_note,
        ];

        $dbTask->update($task);

        return redirect('/admin/tasks');
    }

    public function render()
    {
        return view('livewire.admin.task.review-task', [
            'file' => $this->file,
            'picture' => $this->picture,
        ])->layout('livewire.layout.admin');
    }
}
