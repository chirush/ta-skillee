<?php

namespace App\Livewire\Admin\Task;

use Livewire\Component;
use App\Models\Task;
use App\Models\Intern;
use Illuminate\Support\Facades\Auth;

class EditTask extends Component
{
    public $taskId;
    public $intern;
    public $title;
    public $description;
    public $deadline;

    public function mount($id)
    {
        $task = Task::findOrFail($id);
        $this->taskId = $task->id;
        $this->title = $task->title;
        $this->description = $task->description;
        $this->deadline = $task->deadline;

        $this->dataInterns = Intern::where('instructor_id', Auth::user()->id)->get(); 
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'title' => 'required',
            'description' => 'required',
            'deadline' => 'required|date',
        ]);
    }

    public function editTask()
    {
        $dbTask = Task::findOrFail($this->taskId);

        $validatedData = $this->validate([
            'title' => 'required',
            'description' => 'required',
            'deadline' => 'required|date',
        ]);

        $task = [
            'title' => $this->title,
            'description' => $this->description,
            'deadline' => $this->deadline,
        ];
        
        if (!is_null($this->intern)) {
            $task['intern_id'] = $this->intern;
        }

        $dbTask->update($task);

        return redirect('/admin/tasks');
    }

    public function render()
    {
        return view('livewire.admin.task.edit-task', [
            'dataInterns' => $this->dataInterns,
        ])->layout('livewire.layout.admin');
    }
}
