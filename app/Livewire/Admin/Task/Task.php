<?php

namespace App\Livewire\Admin\task;

use Livewire\Component;
use App\Models\Task as Tasks;
use Illuminate\Support\Facades\Auth;

class Task extends Component
{
    public $dataTasks;

    public function mount()
    {
        $this->dataTasks = Tasks::join('interns', 'tasks.intern_id', '=', 'interns.id')
        ->where('interns.instructor_id', '=', Auth::user()->id)
        ->select('tasks.*', 'interns.name as intern_name')
        ->get();
    }

    public function render()
    {
        return view('livewire.admin.task.task', [
            'dataTasks' => $this->dataTasks,
        ])->layout('livewire.layout.admin');
    }
}
