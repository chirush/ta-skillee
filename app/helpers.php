<?php

use App\Models\Attendance;
use App\Models\Message;
use App\Models\Application;
use App\Models\Branch;
use App\Models\Task;
use Carbon\Carbon;

function hasAttendedToday($internId) {
    $todayDate = Carbon::today();
    return Attendance::where('intern_id', $internId)
        ->whereDate('created_at', $todayDate)
        ->exists();
}

function unreadMessageCount() {
    return Message::where('status', 'Unread')
        ->count();
}

function pendingApplicationCount() {
    return Application::where('status', 'Pending')
        ->count();
}

function getAllBranch() {
    return Branch::all();
}

function countInProgressAndRevisedTasks() {
    return Task::whereIn('status', ['In Progress', 'Revised'])->count();
}

function countUnderReviewTasks() {
    return Task::where('status', 'Under Review')->count();
}
?>
