<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InstanceFaculty extends Model
{
    use HasFactory;
    protected $table = 'instance_faculties';
    protected $primaryKey = 'id';

    protected $fillable = [
        'instance_id', 'name',
    ];
}
