<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InstanceProgram extends Model
{
    use HasFactory;
    protected $table = 'instance_programs';
    protected $primaryKey = 'id';

    protected $fillable = [
        'instance_id', 'instance_faculties', 'name',
    ];
}
