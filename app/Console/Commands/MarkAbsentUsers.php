<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\Attendance;

class MarkAbsentUsers extends Command
{
    protected $signature = 'attendance:mark-absent';
    protected $description = 'Mark users as absent if they have not attended today';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $today = Carbon::today()->toDateString();
        $users = DB::table('interns')->get();

        foreach ($users as $user) {
            $hasAttended = DB::table('attendances')
                ->where('intern_id', $user->id)
                ->whereDate('date', $today)
                ->exists();

            if (!$hasAttended) {
                Attendance::create([
                    'intern_id' => $user->id,
                    'status' => 'Tidak Hadir',
                    'date' => $today,
                    'latitude' => 0,
                    'longitude' => 0,
                    'time' => Carbon::now()->toTimeString(),
                ]);
            }
        }

        $this->info('Absent users have been marked.');
    }
}
