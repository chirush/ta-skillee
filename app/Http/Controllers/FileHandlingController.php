<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Attendance;
use App\Models\Intern;
use App\Models\Branch;
use App\Models\Report;
use App\Models\Task;
use App\Models\Message;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManager;
use Intervention\Image\Drivers\GD\Driver;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class FileHandlingController extends Controller
{
    public function addAttendance(Request $request)
    {
        $timezone = new \DateTimeZone('Asia/Jakarta');
        
        $now = new \DateTime('now', $timezone);
        $dateTime = strtotime($request->input('date'));
        $date = date('Y-m-d', $dateTime);
        $time = $now->format('H:i:s');
        $internId = Auth::guard('intern')->user()->id;

        $manager = new ImageManager(new Driver());
        $randomFileName = Str::random(40) . '.jpg';
        $image = $manager->read($request->file('picture'));
        $filePath = 'public/attendance/' . $randomFileName;
        Storage::put($filePath, (string) $image->scaleDown(height: 400)->encode());

        if ($now->format('H') >= 9 && $request->input('status') == "Hadir") {
            $status = 'Terlambat';
        } else {
            $status = $request->input('status');
        }

        $attendance = Attendance::create([
            'intern_id' => $internId,
            'description' => $request->input('description'),
            'latitude' => $request->input('latitude'),
            'longitude' => $request->input('longitude'),
            'date' => $date,
            'time' => $time,
            'status' => $status,
            'picture' => $randomFileName
        ]);

        $flashMessage = 'Presensi berhasil dicatat.';
        if ($status === 'izin') {
            $flashMessage = 'Presensi berhasil dicatat dengan status izin.';
        } else if ($status === 'sakit') {
            $flashMessage = 'Presensi berhasil dicatat dengan status sakit.';
        } else if ($status === 'Terlambat') {
            $flashMessage = 'Presensi berhasil dicatat namun Anda terlambat.';
        }

        return redirect('/internship/attendances')->with('status', $flashMessage);
    }
    
    public function addReport(Request $request)
    {
        $internId = Auth::guard('intern')->user()->id;

        $manager = new ImageManager(new Driver());
        $randomFileName = Str::random(40) . '.jpg';
        $image = $manager->read($request->file('picture'));
        $filePath = 'public/report/' . $randomFileName;
        Storage::put($filePath, (string) $image->scaleDown(height: 800)->encode());

        $report = Report::create([
            'intern_id' => $internId,
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'picture' => $randomFileName,
            'date' => $request->input('date'),
        ]);

        return redirect('/internship/reports')->with('status', 'Laporan berhasil ditambah.');
    }

    public function editReport(Request $request, $id)
    {
        $internId = Auth::guard('intern')->user()->id;

        $report = Report::where('id', $id)->where('intern_id', $internId)->firstOrFail();

        if ($request->hasFile('picture')) {
            $manager = new ImageManager(new Driver());
            $randomFileName = Str::random(40) . '.jpg';
            $image = $manager->read($request->file('picture'));
            $filePath = 'public/report/' . $randomFileName;
            Storage::put($filePath, (string) $image->scaleDown(height: 800)->encode());

            $report->picture = $randomFileName;
        }

        $report->title = $request->input('title');
        $report->description = $request->input('description');
        $report->date = $request->input('date');

        $report->save();

        return redirect('/internship/reports')->with('status', 'Laporan berhasil diperbarui.');
    }


    public function uploadResume(Request $request)
    {
        $request->validate([
            'resume' => 'required|mimes:pdf|max:2048',
        ]);

        $extension = $request->file('resume')->getClientOriginalExtension();
        $randomFileName = Str::random(40) . '.' . $extension;
        
        $request->file('resume')->storeAs('public/resumes', $randomFileName);

        $intern = Intern::findOrFail(auth()->guard('intern')->user()->id);
        $intern->resume = $randomFileName;
        $intern->save();

        return redirect()->back()->with('status', 'Resume berhasil diupload.');
    }

    public function uploadPicture(Request $request)
    {
        $request->validate([
            'picture' => 'required|max:2048',
        ]);

        $manager = new ImageManager(new Driver());
        $randomFileName = Str::random(40) . '.jpg';
        $image = $manager->read($request->file('picture'));
        $filePath = 'public/picture/' . $randomFileName;
        Storage::put($filePath, (string) $image->cover(400, 400)->encode());
        
        $intern = Intern::findOrFail(auth()->guard('intern')->user()->id);
        $intern->picture = $randomFileName;
        $intern->save();

        return redirect()->back()->with('status', 'Foto berhasil diupload.');
    }

    public function markAsRead($id)
    {
        $dbMessages = Message::findOrFail($id);

        $message = [
            'status' => "Read",
        ];

        $dbMessages->update($message);

        return redirect()->back();
    }

    public function updateTask(Request $request, $id)
    {
        $internId = Auth::guard('intern')->user()->id;

        $task = Task::where('id', $id)->firstOrFail();

        if ($request->hasFile('file')) {
            $fileName = Str::random(40) . '.' . $request->file('file')->getClientOriginalExtension();
            $filePath = $request->file('file')->storeAs('public/task', $fileName);

            $task->file = $fileName;
        }

        if ($request->hasFile('picture')) {
            $manager = new ImageManager(new Driver());
            $randomFileName = Str::random(40) . '.jpg';
            $image = $manager->read($request->file('picture'));
            $filePath = 'public/task/' . $randomFileName;
            Storage::put($filePath, (string) $image->scaleDown(height: 800)->encode());

            $task->picture = $randomFileName;
        }

        $task->report = $request->input('report');
        $task->status = "Under Review";
        $task->save();

        return redirect('/internship/tasks')->with('status', 'Tugas berhasil diperbarui.');
    }

    public function generateCertificate($id)
    {
        $intern = Intern::findOrFail($id);
        $branch = Branch::findOrFail($intern->branch_id);

        $indonesianMonths = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

        function formatDate($date, $months)
        {
            $dateComponents = explode('-', $date);
            $year = $dateComponents[0];
            $month = intval($dateComponents[1]) - 1;
            $day = intval($dateComponents[2]);
            return $day . ' ' . $months[$month] . ' ' . $year;
        }

        $formattedDateStart = formatDate($intern->date_start, $indonesianMonths);
        $formattedDateEnd = formatDate($intern->date_end, $indonesianMonths);

        $todayDate = date('Y-m-d');
        $formattedTodayDate = formatDate($todayDate, $indonesianMonths);

        $program = $intern->level == "University" ? "Program Magang Universitas" : "Program Magang SMA/SMK";

        $humanResources = Auth::user()->name;

        $templatePath = public_path('/certificate_template.png');
        $manager = new ImageManager(new Driver());
        $img = $manager->read($templatePath);

        $textLines = [
            'nameLine' => strtoupper($intern->name),
            'firstLine' => "Yang telah mengikuti $program di $branch->name,",
            'secondLine' => "pada tanggal $formattedDateStart s/d $formattedDateEnd dan yang bersangkutan",
            'thirdLine' => "dinyatakan memenuhi persyaratan dan peraturan yang berlaku di $branch->name,",
            'forthLine' => "sehingga kepadanya diberikan sertifikat.",
            'dateLine' => "Cianjur, $formattedTodayDate",
            'HRNameLine' => strtoupper($humanResources),
            'HRLine' => "Human Resources $branch->name"
        ];

        $textCoordinates = [
            'nameLine' => 457,
            'firstLine' => 574,
            'secondLine' => 611,
            'thirdLine' => 648,
            'forthLine' => 685,
            'dateLine' => 799,
            'HRNameLine' => 973,
            'HRLine' => 1010
        ];

        $calibri = public_path('/fonts/calibri.ttf');
        $calibriBold = public_path('/fonts/calibri bold.ttf');

        foreach ($textLines as $key => $text) {
            $img->text($text, 960, $textCoordinates[$key], function($font) use ($key, $calibri, $calibriBold) {
                $font->file($key === 'nameLine' ? $calibriBold : $calibri);
                $font->size($key === 'nameLine' ? 90 : 30);
                $font->color('#232323');
                $font->align('center');
                $font->valign('middle');
            });
        }

        $randomFileName = Str::random(40) . '.png';
        $filePath = storage_path('app/public/generated_certificates/' . $randomFileName);
        $img->save($filePath);

        return response()->download($filePath)->deleteFileAfterSend(true);
    }
}
