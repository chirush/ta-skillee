<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;

class UserController extends Controller
{
    protected $baseUrl;

    public function __construct()
    {
        $this->baseUrl = env('BASE_URL');
    }

    public function createUser(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required',
            'role' => 'required',
            'phone' => 'required|unique:users,phone',
        ]);

        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer',
                'Accept' => 'application/json',
            ],
        ]);

        $response = $client->request('POST', $this->baseUrl . 'api/add-user',
        [
            'json' => [
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
                'role' => $request->role,
                'phone' => $request->phone,
            ]
        ]);

        $statusCode = $response->getStatusCode();
        $body = $response->getBody();

        $data = json_decode($body);

        return redirect('/register')->with('status', 'User has been registered.');
    }
}
