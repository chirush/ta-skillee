<?php

namespace App\Http\Controllers\api\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Resources\FormatPostResource;

class UserController extends Controller
{
    public function getAllUser()
    {
        $dataUser = User::all();

        return [
            'dataUser' => $dataUser,
        ];
    }

    public function getAllInstructor()
    {
        $dataUser = User::where('role', 'Instructor')->get();

        return [
            'dataUser' => $dataUser,
        ];
    }

    public function createUser(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required',
            'role' => 'required',
            'phone' => 'required|unique:users,phone',
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password,
            'role' => $request->role,
            'phone' => $request->phone,
        ]);
    }
}
