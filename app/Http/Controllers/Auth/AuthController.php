<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\Intern;
use Session;
use GuzzleHttp\Client;
use Mail;
use App\Mail\VerificationEmail;


class AuthController extends Controller
{
    protected $baseUrl;

    public function __construct()
    {
        $this->baseUrl = env('BASE_URL');
    }

    public function verifyEmail($token)
    {
        $intern = Intern::where('verification_token', $token)->first();

        if (!$intern) {
            abort(404);
        }

        $intern->email_verified_at = now();
        $intern->verification_token = null;
        $intern->save();

        return redirect('/login')->with('success', 'Your email has been verified.');
    }

    public function resendVerificationEmail(Request $request)
    {
        $intern = Intern::where('email', $request->email)->first();

        if ($intern) {
            $intern->verification_token = Str::random(40);
            $intern->save();

            Mail::to($intern->email)->send(new VerificationEmail($intern));

            return redirect()->back()->with('success', 'Verification email has been resent.');
        }
    }

    public function logout()
    {
        if (Auth::guard('intern')->check()) {
            Auth::guard('intern')->logout();
        } else {
            Auth::logout();
        }
        return redirect('/');
    }
}